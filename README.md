# bidiRsync

Python lib & tool for *bidirectional* synchronization between directories, 
* Based on rsync and rsh
* Can be used as a python module
* Can be used as a standalone Python script
* Assumptions on remote host: rsync (daemon not necessary), ls, rm, rmdir, ssh, cat, bash
* Assumptions on local host:  the same + python 3.6 

### Main features:
* Bidirectional synchronization, based on rsync
* Windows + cygwin + bash + rsync
* Linux
* Windows long file-names syntax: r'\\?\'  
* the status of previous synchronization is kept in a file, to detect special cases
* special cases: 
   *   File deletion, rename (detected through inode) on one end 
   *   are reproduced on the other end,
   *   unless conflict with the actions on the other end
   *   conflicts: files that are modified differently on both ends
* optimization of directory structure to keep them as synchronized as possible   
* logs (time stamped)
* Multiple synchronization on same repository, e.g. with different hosts
*  moving a directory does not impact the synchronization status
*   .birsync-ignore file, at the root of the source directory, specifies files/Folders to be ignored.
*   self test
*   files/directories with space and accent etc letters.
*   --help for short help
*   --full-help for exhaustive help
*   --dry-run to look at what WOULD happen, without doing it
    
Used to synchronize a PC with a Synology NAS, as a lightweight replacement for Cloud-Station-Drive 
Also used to synchronize between external and internal HDD

---------------------------------------------------------------------
### Outline of algorithm

Step 1: build status of files
 * rsync --dryrun source destination
 * rsync --dryrun destination source
 * the output of dryrun is parsed, and leads to the following:
 * list of files, each with the dryrun "decision", and the reason for it
 * list of directories
 * Only the files and directories that need a transfer are listed 
 

Step 2: get the current and previous status for each file 
 * 'status' for a file is the output of ls -algi
 * 'current' is now, it is read once for the whole tree with a recursive call to ls
 * 'previous' is 'at the time of the last synchronization'
 *  it was stored, directory per directory, in a file, and read from that file 

    
Step 3: Decision
 * for each file, decide about the action 
 * execute it (delete, move)
 * add it to list of Files that need transfer in one or the other direction (or none)
   
 
Step 4:
 * rsync source destination explicit-list-of-files-to-direction
 * rsync destination source explicit-list-of-files-from-direction
 * NB: the files are kept, as a log of the actual actions
   
 
Step 5:
 * synchronize the directory structure by removing the EMPTY directorie that are available
 * on one end and not in the other
 * Due to ignored files, it is difficult to know if a directory is realy empty without testing
      
Step 6:
 * build the new "previous" status on source and destination
    

---------------------------------------------------------------------
### Decision algorithm (Step 3)

After comparing PREVIOUS and CURRENT status
Each file can be in one of the following states, with their 1 char display:

    C Created       i.e. was not existing in the previous synchronization 
    U Unmodified    
    M Modified 
    D Deleted       i.e. was available in PREVIOUS and is absent in CURRENT
    V Moved-OUT     i.e. was available in PREVIOUS, has changed to 'Renamed' aka is the old name     
    R Renamed-IN    i.e. was not in PREVIOUS, is available in CURRENT, this is the new name.   
    A Absent        not existing in PREVIOUS and CURRENT
    E ERROR 


The potential Decision for each file, with their 1 char display:

    Skip     = rsync will not change this file on either ends, because unmodified
    Src2Dst  > rsync will copy from source to destination
    Dst2Src  < rsync will copy from source to destination
    Conflict C the Source file will be renamed locally, then files will be synchronized in both directions
    RmSrc    D Delete on the source, because was deleted on the destination
    RmDst    d delete on the destination, because was deleted on the source
    MvSrc    M move (i.e. change name) on the source, because was renamed on destination
    MvDst    m Move (i.e. change name) on the destination, because was renamed on source
    Null     # No decision is necessary, rsync will not manage this file
    Error    ! is an internal error, it should never happen


Which leads to the following table:

                      destination
                      C U M D V R A
                      
       S  Created     C C C > > C >   
       o  Unmodified  C = < D = C >   
       u  Modified    C > C > > C C   
       r  Deleted     < d < # # < #   
       c  V Moved out < = < # # < #   
       e  Renamed in  C C C > > C m   
          Absent      < < < # # M #   
      
 Cases that are not obvious:
 
    V+U: old-file has been moved out, to new-file
        if new-file is in R+A case and on source, new-file == old-file (same date, same size)
        then decision = #  there is nothing to do, because the decision on new-file is m (move src)
        else decision = D (del on source) because the decisionon new-file is > 
        
    R+A: on source, old-file has been moved out, to new-file
        if old-file is in V+U case and, on source new-file == old-file
        then decision = M (move src)
        else decision = > (copy to dest)

  
---------------------------------------------------------------------
### Display in verbose mode

 in verbose mode, the decision for each file is displayed with status line of the following format
 decision: TtRCc?  'filename'  -->/<-- 'alternate-filename'
 with:
 
     T = Transition on source
     t = transition on destination
     R = rsync dryrun char for the file, in the source to destination direction
     C = Change reason on source 
     r = rsync dryrun char for the file, in the destination to source direction 
     c = change reason on destination
     D = decision
     
 The 'alternate filename' is the new/old name for the file
 
 where C/c change reasons is:
 
     . = no change
     i = inode
     d = date
     n = path
     s = size
     o = rsync detected a change on source in the --> direction
     O = rsync detected a change on destination in --> direction
     p = rsync detected a change on source in the <-- direction
     P = rsync detected a change on destination in <-- direction
 

---------------------------------------------------------------------
### HOW TO to initiate the SSH connection with the linux host

     ssh-keygen -t rsa -b 2048
     ssh-copy-id username@hostname -p portnumber
     ssh ssh://username@hostname:portnumber # will ask for username password the first time


---------------------------------------------------------------------
### Management of .birsync-ignore

This file is used only at the root of the synchronized directories,
because rsync cannot manage it with several directories

if you really want a finer grain, you need to rsync directory per directory
The file is always read on the local host, independently on the synchronization direction

---------------------------------------------------------------------
### Management of history database

1 file is created in *root directory*, on each host

     .birsync_{host}_{hash}_to_{peer}_{peer_hash} 
     where {host} and {peer} are the hostnames
     {hash} and {peer_hash} are SHA1 hashes of the root path of synchronization
     
This naming allows:
* previous status can be used if the source/destination order is changed.
* previous status are kept different, even is the host is identical, since the root dirs cannot be also identical
* status files are different if the tree synchronized is different

The status files hold the status of the directory after the LAST synchronization
So, they are updated when the synchronization is completed.
If not prevented by the ignore rules, these files will be synchronized.
since the update is done after synchronization, the synchronized version is overwritten.

The state of each file (see "Decision algorithm") is done by comparing 
the .rsync_remote/local_dir_{hostname} with the current status BEFORE the synchronization
       

---------------------------------------------------------------------
### Differences between source and destination

The synchronization is bidirectional, so, there are very little differences between source and destination:
* in case of conflict,  destination is unchanged and source is moved to file_{stamp}.extension 

---------------------------------------------------------------------
### Examples of SELF TESTS
  the same arguments can always be used with --parse-only and/or --dry-run and --verbose-XXX options
 
    --self-test  
    # no directory specified: a temp directory is created, and src, dst directories in it
    # format is src_{'%Y-%m-%d_T%H-%M-%S'}
    
    --self-test --keep-test-file 
    # idem. test files are kept
  
    --self-test --source "C:\Windows\Temp" --dest "C:\Windows\Temp" 
    # src and dst directories are created in the specified directories
  
    --self-test --source "C:\Windows\Temp" --dest "/tmp"   --dest-host "my_host" --user "my_user" --port 2222  
    --self-test --dest "C:\Windows\Temp" --source "/tmp" --source-host "my_host" --user "my_user" --port 2222 
    # self-test is OK in both directions

---------------------------------------------------------------------
### Examples of normal usages
   the same arguments can always be used with --parse-only and/or --dry-run and --verbose-XXX options

    --source "D:\\win_dir" --dest "/volume1/linux_dir"  --dest-host "my_host" --user "my_user" --port 2222
    --dest "D:\\win_dir\\" --source "/volume1/linux_dir/"  --source-host "my_host" --user "my_user" --port 2222
      # both directions are possible
      # assuming the 1st host is running windows and cygwin
      # the ending \\ or / is removed
   
    --source "/cygdrive/c/win_dir2" --dest "/volume1/linux_dir2" --dest-host "my_host" --user "my_user" --port 2322 
      # alternative syntax for windows/cygwin host

[--end of file--]