import os
import subprocess
import re
import tempfile
import dateutil.parser
import platform
import argparse
import shutil
import pathlib
import datetime
import stat
import hashlib


# python -m pip install python-dateutil

# ---------------------------------------------------------------------
# How to initiate the SSH connection with the linux host
# ---------------------------------------------------------------------
# ssh-keygen -t rsa -b 2048
# ssh-copy-id rsyncuser@192.168.1.16 -p 2322
# ssh ssh://rsyncuser@192.168.1.16:2322 # will ask for rsyncuser password
# ---------------------------------------------------------------------

# DONE: --rsync-options
# DONE: remove the error cat: .rsync_remote_dir : No such file or directory
# DONE: add licensing terms and --licence
# DONE: Manage space, &, ', ", Unicode etc in filename and directory names, over SSH, BASH, in both directions
# DONE: get hostname and date for the conflict name
# TODO: Option --reset : removes the .rsync_local files
# DONE: Do --self-tests with a remote host, in both directions
# DONE: Test the existence of the local_dir, which may have not been synchronized
# DONE: manage non usual characters in the remote DIRECTORY names ==> args parameter of subprocess is a LIST
#       because a string with accentuated letters is split incorrectly!
# TODO: add option --ignore-file
# DONE: Bug: files that SHOULD NOT have been synchronized are not reported with --self-test
# DONE: rsync: failed to set times  --> --omit-dir-times
# DONE: Correctly set the chmod: -p -g --chmod=ugo=rwX
# DONE: Add --parse-only
# DONE: add --full-help (partly, need to continue help)
# DONE: Expliquer la creation de .rsync-local-dir et remote-dir
# DONE: Manage Cygwin directory names.
# DONE: Manage windows path with more than 260 chars
# DONE: Optimize directory structure by synchronising hierarchy when possible
# DONE: Only one (large) .rsync_XXX_to_YYY file
# DONE: align decision with the specification for R+U and V+A
# DONE: Minor bug: tentative to delete dirs that were ignored

# Management of .birsync-ignore
# This file is used only at the root of the synchronized directories,
# because rsync cannot manage it with several directories
# if you really want a finer grain, you need to rsync directory per directory
# The file is always read on the local host, independently on the synchronization direction


# ---------------------------------------------------------------------
# class PrintDebug
# ---------------------------------------------------------------------


class PrintDebug:
    """
    Class for versatile print debug
    Everything is also stored in birsync.log file (no rotation of logfile)
    Levels:
         Silent = no messages at all
         Quiet = almost nothing, only major errors
         Normal
         Verbose = when trying to understand what happens
         Debug = for developers
         Deep = even too much for developers

    """
    Silent = 0
    Quiet = 1
    Normal = 2
    Verbose = 3
    Debug = 4
    Deep = 5
    RED_COLOR = '\033[91m'
    END_COLOR = '\033[0m'

    def __init__(self):
        self.filename = datetime.datetime.today().strftime('birsync-%Y-%m-%d_T%H-%M-%S.log')
        self.debugF = open(self.filename, 'w', encoding='utf8')
        self.verbose = 2

    def set_level(self, verbose):
        self.verbose = verbose

    def print(self, msg, level=Normal):
        if level <= self.verbose:
            if self.verbose > 0:
                if msg.startswith("ERROR"):
                    print(f"{PrintDebug.RED_COLOR}{msg}{PrintDebug.END_COLOR}")
                else:
                    print(msg)
            print(msg, file=self.debugF)


Printer = PrintDebug()


def find_exec(filename):
    """find the executable 'filename' in the path configured"""
    # look into traditional locations if the actual path is not delivered in cmdline
    if platform.system() == "Windows":
        options = ('c:\\cygwin64\\bin',
                   'd:\\cygwin64\\bin',
                   'e:\\cygwin64\\bin',
                   'c:\\cygwin\\bin',
                   'd:\\cygwin\\bin',
                   'e:\\cygwin\\bin')
        postfix = '.exe'
        sep = '\\'
    else:
        options = ("/sbin", "/bin", "/usr/sbin", "/usr/bin", "/usr/bin/local")
        postfix = ''
        sep = '/'
    for loc_dir in options:
        file = loc_dir + sep + filename + postfix
        if os.path.exists(file):
            return file


class UniqueStamp:
    def __init__(self):
        self.counter = 0

    def stamp(self):
        stamp = datetime.datetime.today().strftime('%Y-%m-%d_T%H-%M-%S')
        self.counter += 1
        return f"Counter {stamp} {self.counter}"


Stamper = UniqueStamp()

# ---------------------------------------------------------------------
# class Host
# ---------------------------------------------------------------------


class Host:
    """
    Describes an Host,
    implements a few commands
    by default, execution is LOCAL on the same host
    see derived class ShhHost for remote
    """

    def __init__(self, context, directory, user=None, hostname=None, port=None, bash=None, ssh=None, rsync=None,
                 postfix=None, local_hostname=None):
        self.context = context
        self.user = user
        self.hostname = hostname
        self.port = port

        self.windows = False
        if self.hostname is None:
            self.windows = platform.system() == "Windows"
        self.hostname_for_hash = hostname or local_hostname or platform.node()

        self.directory = Host.local_filename(directory)
        self.directory = re.sub(r'([\\/])$', '', self.directory)

        dir_bytes = self.directory.encode(encoding='UTF-8', errors='strict')
        self.hash_stamp = hashlib.sha1(dir_bytes).hexdigest()

        self.bash = bash or find_exec("bash")
        self.ssh = ssh or find_exec("ssh")
        self.rsync_exec = rsync or find_exec("rsync")
        self.rsync_path = self.build_rsync_path()
        self.postfix = postfix or (self.hostname_for_hash +
                                   '-' + datetime.datetime.today().strftime('%Y-%m-%d_T%H-%M-%S'))
        self.ignore_filename = ".birsync-ignore"
        self.src_to_dst_files = ""  # file in src --> Dest direction
        self.dst_to_src_files = ""  # file in Dest --> src direction
        self.dir_descriptor = None  # A BirsyncDir

    @staticmethod
    def create(context, directory, user=None, hostname=None, port=None, bash=None, ssh=None,
               rsync=None, postfix=None, local_hostname=None):
        """creates the appropriate host, depending on the value of parameters passed"""
        if hostname is None:
            return Host(context, directory, user=user, bash=bash, ssh=ssh, rsync=rsync, postfix=postfix,
                        local_hostname=local_hostname)
        return ShhHost(context, directory, user=user, hostname=hostname, port=port, bash=bash, ssh=ssh,
                       local_hostname=local_hostname, rsync=rsync, postfix=postfix)

    def __rep__(self):
        return str(self.hostname) + ':' + str(self.port)

    def fullname(self):
        un = self.user + '@' if self.user is not None else ""
        return un + str(self.hostname) + ':' + str(self.port)

    def is_local(self):
        """:returns True if self is the host where birsync is executed"""
        return self.hostname is None

    def build_rsync_path(self):
        """:returns the path part of rsync, following rsync syntax"""
        if self.windows:
            return ''
        remote = ""
        if self.user is not None:
            remote += self.user + "@"
        if self.hostname is not None:
            remote += self.hostname + ':'
        return remote

    # Management of path syntax
    # by default, all paths are expressed with local syntax, which is the standard for python
    # conversion to linux is done only when executing bash, rsh or rsync with a linux syntax

    Mode_normal = 1  # No special char
    Mode_accent = 2  # at least 1 accent letter, with or without a quote
    Mode_quote = 3  # one quote, no accent

    @staticmethod
    def check_accents(filename):
        result = Host.Mode_normal
        for letter in filename:
            if ord(letter) > 128:
                return Host.Mode_accent
            if letter == '"' or letter == "'":
                result = Host.Mode_quote
        return result

    @staticmethod
    def unquote_filename(filename):
        p = re.match(r"^'(.*)'$", filename)
        if p:
            filename = p.group(1)
        p = re.match(r'^"(.*)"$', filename)
        if p:
            filename = p.group(1)
        return filename

    @staticmethod
    def quoted_linux_filename(filename):
        """
        :returns filename with the linux syntax.
        manages ' ', '"', "'", accent-letters '&' in the filename
        assumes cygwin for windows systems
        used with bash or rsh
        """
        if platform.system() == "Windows":
            filename = re.sub(r"^([a-zA-Z]):(/|\\*)", "/cygdrive/\\1/", filename)
            filename = re.sub(r'\\', '/', filename)

        accent = Host.check_accents(filename)
        # CAVEAT:
        # This behavior is very strange, probably due to parsing of the string within check-process
        # the quote rules are NOT the same depending on accent letters or not !
        # this is a UGLY TURNAROUND

        if accent == Host.Mode_normal:
            return "'" + filename + "'"
        if accent == Host.Mode_accent:
            return filename
        #  must be Mode_quote
        return '"' + filename + '"'

    @staticmethod
    def local_filename(filename):
        """
        :param filename: the file being managed
        :return: filename with the local syntax, suitable for Python.
        """
        if platform.system() == "Windows":
            filename = re.sub(r"^/cygdrive/([a-zA-Z])/", "\\1:/", filename)
            filename = re.sub(r'/', '\\\\', filename)
            if len(filename) > 250 and not filename[0:4] == "\\\\?\\":
                filename = "\\\\?\\" + filename
                # this removes the path limitation of 256 char
                # see https://docs.microsoft.com/fr-fr/windows/win32/fileio/naming-a-file#maximum-path-length-limitation
        return filename

    @staticmethod
    def exec_and_get_result(cmd_line, msg):
        Printer.print(f"execute {msg} {cmd_line}", Printer.Debug)
        res = ""
        try:
            res = subprocess.check_output(cmd_line, encoding='utf8', stderr=subprocess.STDOUT)
        except subprocess.CalledProcessError as inst:
            # DONE: check normal terminations
            Printer.print(f"ERROR execute {msg} {cmd_line}")
            Printer.print(f"ERROR message {inst.output}")
        return res

    def execute_linux_cmd(self, cmd_line):
        """
        executes the bash command contained in cmd_line
        :return: the result
        """
        return Host.exec_and_get_result(cmd_line, "local")

    @staticmethod
    def get_mode_str(fd_mode):
        mode = 'd' if stat.S_ISDIR(fd_mode) else '-' if stat.S_ISREG(fd_mode) else 'X'
        imode = stat.S_IMODE(fd_mode)
        mode += 'r' if stat.S_IRUSR & imode else '-'
        mode += 'w' if stat.S_IWUSR & imode else '-'
        mode += 'x' if stat.S_IXUSR & imode else '-'

        mode += 'r' if stat.S_IRGRP & imode else '-'
        mode += 'w' if stat.S_IWGRP & imode else '-'
        mode += 'x' if stat.S_IXGRP & imode else '-'

        mode += 'r' if stat.S_IROTH & imode else '-'
        mode += 'w' if stat.S_IWOTH & imode else '-'
        mode += 'x' if stat.S_IXOTH & imode else '-'
        return mode

    #
    # stat.S_ISUID    Set UID bit.
    # stat.S_ISGID    Set-group-ID bit. This bit has several special uses.
    # stat.S_ISVTX    Sticky bit.
    # stat.S_IRWXU    Mask for file owner permissions.
    # stat.S_IRUSR    Owner has read permission.
    # stat.S_IWUSR    Owner has write permission.
    # stat.S_IXUSR    Owner has execute permission.
    # stat.S_IRWXG       Mask for group permissions.
    # stat.S_IRGRP    Group has read permission.
    # stat.S_IRWXO    Mask for permissions for others (not in group).
    # stat.S_ENFMT    System V file locking enforcement.
    # stat.S_IREAD    Unix V7 synonym for S_IRUSR.
    # stat.S_IWRITE   Unix V7 synonym for S_IWUSR.
    # stat.S_IEXEC  Unix V7 synonym for S_IXUSR.

    # statinfo = os.stat('somefile.txt')
    # st_mode  File mode: file type and file mode bits (permissions).
    # st_ino            the inode number on Unix,
    # st_dev            Identifier of the device on which this file resides.
    # st_nlink          Number of hard links.
    # st_uid   User identifier of the file owner.
    # st_gid   Group identifier of the file owner.
    # st_size  Size of the file in bytes, if it is a regular file or a symbolic link.
    # Timestamps:
    # st_atime          Time of most recent access expressed in seconds.
    # st_mtime  Time of most recent content modification expressed in seconds.
    # st_ctime          the time of most recent metadata change on Unix,  of creation on Windows, expressed in seconds.
    # st_atime_ns       Time of most recent access expressed in nanoseconds as an integer.
    # st_mtime_ns Time of most recent content modification expressed in nanoseconds as an integer.
    # st_ctime_ns  the time of most recent metadata change on Unix,   the time of creation on Windows,  nanoseconds.

    #  9851624185293031 -rwx------  1 Domain Users  531 2018-08-14 11:30:16.000000000 +0200 'Config- - Copy.txt'

    def build_ls_algi_file(self, filename):
        filename = self.local_filename(filename)
        statinfo = os.stat(filename)
        mode = Host.get_mode_str(statinfo.st_mode)
        name = pathlib.PurePath(filename).name
        uname = statinfo.st_uid
        user = uname  # os.get_user_name(uname)
        group = statinfo.st_gid  # os.get_group_name(statinfo.st_gid)
        my_date: datetime.datetime = datetime.datetime.fromtimestamp(statinfo.st_mtime)
        iso_date = my_date.isoformat(sep=" ")

        if ' ' in name:
            if "'" in name:
                name = '"' + name + '"'
            else:
                name = "'" + name + "'"
        res = f"{statinfo.st_ino} {mode} {user} {group} {statinfo.st_size} {iso_date} +0100 {name}\n"
        return res

    def ls_algi_file(self, filename):
        if self.file_exists(filename):
            return self.build_ls_algi_file(filename)
        return None

    def build_ls_algi_dir(self, dir_name, recursive=False, start='.'):
        res = ""
        dir_name = self.local_filename(dir_name)
        file_list = os.listdir(dir_name)
        if recursive:
            res += start + ":\n"
        res += "total 0\n"

        dir_list = []
        for file in file_list:
            path = self.local_filename(dir_name + '/' + file)
            res += self.build_ls_algi_file(path)
            if os.path.isdir(path) and recursive:
                dir_list.append(file)

        res += "\n"
        for loc_dir in dir_list:
            res += self.build_ls_algi_dir(dir_name + '/' + loc_dir, recursive=True, start=start + '/' + loc_dir)

        return res

    def build_new(self, dir_name, local_file, msg, recursive=False):
        """Create the new list of files after the synchronization
           CAVEAT: dirs could be not present, if only on the peer
        """
        # DONE: Replace ls and bash by fstat
        if not os.path.isdir(self.local_filename(dir_name)):
            #  we assume that local_dir exists only on the remote_host
            return
        path = self.local_filename(dir_name + "\\" + local_file)
        res = self.build_ls_algi_dir(dir_name, recursive=recursive)
        fo = open(path, 'w', encoding='utf8')
        fo.write(msg)
        fo.write(res)
        fo.write("\n# end of file\n")
        fo.close()
        return

    # def build_new_iterative(self, local_dir, dir_list, local_file):
    #     # self.build_local_new(local_dir)
    #     for cur_dir in dir_list:
    #         cur_dir = '' if cur_dir == '' else '/' + cur_dir
    #         self.build_new(local_dir + cur_dir, local_file)

    def build_new_recursive(self, local_dir, local_file, msg):
        self.build_new(local_dir, local_file, msg, recursive=True)

    def get_current(self, path, src_path, dst_path, files_hash, inodes_hash, ignore=True):
        Printer.print(f"get_current local '{path}' ", Printer.Debug)
        ret_val = self.build_ls_algi_dir(path, recursive=True)
        # cmd_line = "ls -algiR --time-style=full-iso " + self.quoted_linux_filename(path)
        # ret_val = self.execute_linux_cmd(cmd_line)
        return self.context.parse_ls_output(ret_val, src_path, dst_path, files_hash, inodes_hash, ignore=ignore)

    def get_previous(self, path, file, src_path, dst_path, files_hash, inodes_hash, cur_dir):
        filename = self.local_filename(path + '/' + file)
        if os.path.exists(filename):
            Printer.print(f"get_previous local '{filename}' ", Printer.Debug)
            with open(filename, 'r', encoding='utf8') as content_file:
                content = content_file.read()
            return self.context.parse_ls_output(content, src_path, dst_path, files_hash, inodes_hash,
                                                current_dir=cur_dir)
        Printer.print(f"get_previous local '{path}' / '{file}' absent, skipping", Printer.Debug)
        return []

    def get_previous_recursive(self, path, local_file, src_path, dst_path, files_hash, inodes_hash):
        return self.get_previous(path, local_file, src_path, dst_path, files_hash, inodes_hash, "")

    def get_previous_iterative(self, path, local_file, src_path, dst_path, dir_list, files_hash, inodes_hash):
        """
        :param path:        Path to the root directory of synchronization
        :param local_file:  Name of the file that holds the previous state
        :param src_path:    Path of root directory, for SRC, will be used by parse_ls_output
        :param dst_path:    Path of root directory, for DST, will be used by parse_ls_output
        :param dir_list:    Hash of directories for the root directory, will be iterated upon
        :param files_hash:  Hash of files
        :param inodes_hash: hash of Inodes
        :return: None
        """
        for cur_dir in dir_list:
            if cur_dir != '' and cur_dir[0] == '/':
                cur_dir = cur_dir[1:]  # remove heading /
            if cur_dir != '' and cur_dir[-1:] != '/':
                cur_dir = cur_dir + '/'

            self.get_previous(path, cur_dir + local_file, src_path, dst_path, files_hash, inodes_hash, cur_dir)

    def file_exists(self, path):
        filename = self.local_filename(path)
        return os.path.isfile(filename)

    def dir_exists(self, path):
        filename = self.local_filename(path)
        return os.path.isdir(filename)

    def move_file(self, source, destination, list_only=False):
        src = self.local_filename(source)
        dst = self.local_filename(destination)
        dirname = str(pathlib.PurePath(destination).parent)
        dirn = self.local_filename(dirname)
        self.mkdir(dirn)

        if not os.path.exists(src):
            Printer.print(f"ERROR INTERNAL: mv :The file '{src}' does not exist")
            return
        if list_only:
            Printer.print(f"birsync: WOULD move '{src}' to '{dst}' on local host", Printer.Verbose)
            return
        Printer.print(f"move '{src}' to '{dst}' on local host", Printer.Debug)
        os.rename(src, dst)

    def remove_file(self, path, list_only=False):
        src = self.local_filename(path)
        if list_only:
            Printer.print(f"birsync: WOULD delete {src} on local host", Printer.Verbose)
        elif os.path.exists(src):
            Printer.print(f"rm '{src}' on local host", Printer.Debug)
            os.remove(src)
        else:
            Printer.print(f"ERROR INTERNAL: rm : The file '{src}' does not exist")

    def remove_dir(self, path, verbose_level, list_only=False, empty_only=False):
        src = self.local_filename(path)
        if list_only:
            Printer.print(f"birsync: WOULD delete directory {src} on local host", Printer.Verbose)
        elif os.path.exists(src):
            try:
                Printer.print(f"rmdir '{src}' on local host", Printer.Debug)
                if empty_only:
                    os.rmdir(src)
                else:
                    shutil.rmtree(src)
            except (shutil.Error, OSError):
                if not empty_only:
                    Printer.print(f'ERROR while deleting directory {src}', verbose_level)
                return False
            return True
        else:
            Printer.print(f"ERROR INTERNAL: rm :The file '{path}' does not exist")
            return False

    def build_one_file(self, path, msg: [str] = None):
        src = self.local_filename(path)
        file = open(src, 'w', encoding='utf8')
        file.write("# This is a test file automatically generated Locally\n")
        if msg is None:
            msg = []
        for line in msg:
            file.write(line + '\n')
        file.write("# This is the original test file\n")
        file.close()

    def change_file(self, path, list_only):
        src = self.local_filename(path)
        if list_only:
            Printer.print(f"birsync: WOULD delete {src} on local host", Printer.Verbose)
            return

        Printer.print(f"touch '{src}' on local host", Printer.Debug)
        file = open(src, "a", encoding='utf8')
        file.write(f"\nThis file has been modified by test local automation at {Stamper.stamp()}\n")
        file.close()

    def mkdir(self, path):
        src = self.local_filename(path)
        try:
            os.makedirs(src, mode=0o777, exist_ok=True)
        except OSError:
            Printer.print(f'ERROR while creating temporary test directories')
        Printer.print(f"mkdir '{src}' on local host", Printer.Debug)

    def read_file(self, path, print_error=True):
        src = self.local_filename(path)
        if not os.path.exists(src):
            if print_error:
                Printer.print(f"ERROR: local file {src} is not readable ")
            return ""
        res = ""
        Printer.print(f"read_file '{src}' on local host", Printer.Debug)
        with open(src, 'r', encoding='utf8') as fh:
            for line in fh:
                res = res + line
        return res


class ShhHost(Host):
    """For hosts that are accessed across SSH  """

    def __init__(self, context, directory, user=None, hostname=None, port=None, bash=None, ssh=None,
                 rsync=None, postfix=None, local_hostname=None):
        super().__init__(context, directory, user=user, hostname=hostname, port=port, bash=bash, ssh=ssh,
                         rsync=rsync, postfix=postfix, local_hostname=local_hostname)

    #  example: [ssh ssh://rsyncuser@192.168.1.16:2322 ls -alg '/volume1/Famille/T/dir with é']
    def execute_linux_cmd(self, cmd_line):
        start = "ssh://"
        if self.user is not None:
            start += self.user + "@"
        if self.hostname is not None:
            start += self.hostname
        if self.port is not None:
            start += ':' + str(self.port)
        cmd_line = [self.ssh, start] + cmd_line

        return Host.exec_and_get_result(cmd_line, "remote")

    def build_new(self, local_dir, local_file, msg, recursive=False):
        """Create the new list of files after the synchronization"""
        # DONE: Test the existence of the local_dir, which may have not been synchronized
        loc = self.quoted_linux_filename(local_dir)
        file = self.quoted_linux_filename(local_dir + "\\" + local_file)
        rec = "R" if recursive else ""
        cmd_line = ['if', '[', '-d', loc, ']', ';',
                    'then', 'ls', '-algi' + rec, '--time-style=full-iso', loc, '>', file, ';',
                    'fi']

        self.execute_linux_cmd(cmd_line)
        return

    def get_current(self, path, src_path, dst_path, files_hash, inodes_hash, ignore=True):
        Printer.print(f"get_current ssh   '{path}' ", Printer.Debug)
        cmd_line = ['ls', '-algiR', '--time-style=full-iso', self.quoted_linux_filename(path)]
        ret_val = self.execute_linux_cmd(cmd_line)
        return self.context.parse_ls_output(ret_val, src_path, dst_path, files_hash, inodes_hash, ignore=ignore)

    def ls_algi_file(self, filename):
        fn = self.quoted_linux_filename(filename)
        cmd_line = ['if', '[', '-f', fn, ']', ';',
                    'then', 'ls', '-algi', '--time-style=full-iso', ';',
                    'else', 'echo', 'NON-EXISTING', ';',
                    'fi']
        ret_val = self.execute_linux_cmd(cmd_line)
        if ret_val == 'NON-EXISTING\n':
            return None
        return ret_val

    def get_previous(self, path, filename, src_path, dst_path, files_hash, inodes_hash, cur_dir):
        """
        Get the remote description of files from the previous synchro, as a hash
        """
        Printer.print(f"get_previous ssh   '{path}' / '{filename}' ", Printer.Debug)
        fn = self.quoted_linux_filename(path + '/' + filename)
        cmd_line = ['if', '[', '-f', fn, ']', ';',
                    'then', 'cat', fn, ';',
                    'else', 'echo', 'NON-EXISTING', ';',
                    'fi']
        ret_val = self.execute_linux_cmd(cmd_line)
        if ret_val == 'NON-EXISTING\n':
            return []
        return self.context.parse_ls_output(ret_val, src_path, dst_path, files_hash, inodes_hash, current_dir=cur_dir)

    def file_exists(self, path):
        Printer.print(f"file_exists ssh   '{path}'", Printer.Debug)
        fn = self.quoted_linux_filename(path)
        cmd_line = ['if', '[', '-f', fn, ']', ';',
                    'then', 'echo', 'YES', ';',
                    'else', 'echo', 'NON-EXISTING', ';',
                    'fi']
        ret_val = self.execute_linux_cmd(cmd_line)
        if ret_val == 'NON-EXISTING\n':
            return False
        return True

    def dir_exists(self, path):
        Printer.print(f"file_exists ssh   '{path}'", Printer.Debug)
        fn = self.quoted_linux_filename(path)
        cmd_line = ['if', '[', '-d', fn, ']', ';',
                    'then', 'echo', 'YES', ';',
                    'else', 'echo', 'NON-EXISTING', ';',
                    'fi']
        ret_val = self.execute_linux_cmd(cmd_line)
        if ret_val == 'NON-EXISTING\n':
            return False
        return True

    def move_file(self, source, destination, list_only=False):
        src = self.quoted_linux_filename(source)
        dst = self.quoted_linux_filename(destination)

        dirname = pathlib.PurePath(Host.unquote_filename(dst)).parent.as_posix()
        dirn = self.quoted_linux_filename(dirname)

        if list_only:
            Printer.print(f"birsync: WOULD move '{src}' to '{dst}' on remote host", Printer.Verbose)
            return
        cmd_line = ['mkdir', "-m", "777", '-p', dirn, ';', 'mv', src, dst]
        self.execute_linux_cmd(cmd_line)

    def remove_file(self, path, list_only=False):
        src = self.quoted_linux_filename(path)
        if list_only:
            Printer.print(f"birsync: WOULD delete {src} on remote host", Printer.Verbose)
            return
        cmd_line = ["rm", src]
        self.execute_linux_cmd(cmd_line)

    def remove_dir(self, path, verbose_level, list_only=False, empty_only=False):
        src = self.quoted_linux_filename(path)
        if list_only:
            Printer.print(f"birsync: WOULD delete {src} on remote host", Printer.Verbose)
            return
        if empty_only:
            cmd_line = ["rmdir", "--ignore-fail-on-non-empty", src]
        else:
            cmd_line = ["rm", "-r", src]
        self.execute_linux_cmd(cmd_line)

    def build_one_file(self, path, msg: [str] = None):
        if msg is None:
            msg = []
        src = self.quoted_linux_filename(path)
        str_msg = '"# This is a test file automatically generated Remotely\\\\n'
        for line in msg:
            str_msg += line + '\\\\n'
        str_msg += '# This is the original test file\\\\n"'
        cmd_line = ["echo", "-e", str_msg, ">", src]
        self.execute_linux_cmd(cmd_line)

    def change_file(self, path, list_only):
        src = self.quoted_linux_filename(path)
        if list_only:
            Printer.print(f"birsync: WOULD Change {src} on remote host", Printer.Verbose)
            return
        cmd_line = ['echo', "-e",
                    f"\\nThis file has been modified by remote test automation at {Stamper.stamp()}\\n",
                    ">>", src]
        self.execute_linux_cmd(cmd_line)

    def mkdir(self, path):
        src = self.quoted_linux_filename(path)
        cmd_line = ["mkdir", "-m", "777", "-p", src]
        self.execute_linux_cmd(cmd_line)

    def read_file(self, path, print_error=True):
        src = self.quoted_linux_filename(path)
        cmd_line = ['if', '[', '-f', src, ']', ';',
                    'then', 'cat', src, ';',
                    'else', 'echo', 'NON-EXISTING', ';',
                    'fi']

        ret_val = self.execute_linux_cmd(cmd_line)
        if ret_val == 'NON-EXISTING\n':
            if print_error:
                Printer.print(f"ERROR: ssh file '{src}' is not readable ")
            return ""
        return ret_val


# ----------------------------------------------------
# Transition
# ----------------------------------------------------


class Trans:
    """Describes the transition between the Previous and Current states, in Src or Dst location"""

    def __init__(self, char, value, descriptor):
        self.value = value
        self.char = char
        self.str = descriptor


class Transition:
    Created = Trans("C", 0, "Created")
    Unmodified = Trans("U", 1, "Unmodified")
    Modified = Trans("M", 2, "Modified")
    Deleted = Trans("D", 3, "Deleted")
    Moved = Trans("V", 4, "Moved")
    Renamed = Trans("R", 5, "Renamed")
    Absent = Trans("A", 6, "Absent")
    ERROR = Trans("E", 17, "Error")


# ---------------------------------------------------------------------
# class RsyncChoice
# ---------------------------------------------------------------------
# DONE: replace Enum by Classes


class RSChoice:
    """Describe the decision taken by rsync with a dry-run test"""

    def __init__(self, char, value, descriptor):
        self.value = value
        self.char = char
        self.str = descriptor


class RsyncChoice:
    RsyncSkip = RSChoice(".", 0, "RsyncSkip")
    RsyncChanged = RSChoice("C", 0, "RsyncChanged")
    RsyncNew = RSChoice("+", 0, "RsyncNew")
    RsyncError = RSChoice("E", 0, "RsyncError")


# ---------------------------------------------------------------------
# class Decision
# ---------------------------------------------------------------------
# DONE: replace Enum by Subclasses


class Deci:
    def __init__(self, value, char):
        self.value = value
        self.char = char


class Decision:
    Skip = Deci(0, "=")
    Src2Dst = Deci(1, ">")
    Dst2Src = Deci(2, "<")
    Conflict = Deci(3, "C")
    RmSrc = Deci(4, "D")
    RmDst = Deci(5, "d")
    MvSrc = Deci(6, "M")
    MvDst = Deci(7, "m")
    Null = Deci(8, "#")
    RsyncError = Deci(10, "!")

    @staticmethod
    def from_char(char):
        if char == "=":
            return Decision.Skip
        elif char == ">":
            return Decision.Src2Dst
        elif char == "<":
            return Decision.Dst2Src
        elif char == "C":
            return Decision.Conflict
        elif char == "D":
            return Decision.RmSrc
        elif char == "d":
            return Decision.RmDst
        elif char == "M":
            return Decision.MvSrc
        elif char == "m":
            return Decision.MvDst
        elif char == "#":
            return Decision.Null
        else:
            return Decision.RsyncError


# -------------------------------------------
# Class BiDir
# -------------------------------------------


class BirsyncDir:
    """
    Describes a directory
    relative_path = path relative to the root of the synchronization
    sub_dirs[dirname] = BirsyncDir, for self sub-directories that are not ignored
    tree[relative_path] = BirsyncDir, from the root this hash is shared by all sons
    """

    def __init__(self, dir_name, father):
        self.father = father
        self.dir_name = dir_name
        self.sub_dirs = {}
        self.tree = {} if father is None else father.tree
        self.is_used = False  # Wil be used as a marker to remove empty directories after pruning files

        self.relative_path = dir_name if father is None or father.relative_path == '' else \
            father.relative_path + "/" + dir_name

        if father is not None:
            father.sub_dirs[dir_name] = self
        self.tree[self.relative_path] = self

    def add_subdir(self, subdir):
        # if self.is_ignored_dir(subdir):
        #     return None
        if subdir not in self.sub_dirs:
            self.sub_dirs[subdir] = BirsyncDir(subdir, self)
        return self.sub_dirs[subdir]

    def insert_parts(self, parts, n):
        """
        insert in the appropriate son, and all the intermediates
        but not the final file, which is not a directory by definition
        parts is the result of parts
        n is the index within parts
        """
        length = len(parts)
        if n >= length - 1:
            # only files in the path
            return self

        if parts[n] == '/':
            return self.insert_parts(parts, n + 1)

        if length - 2 < 0:
            Printer.print(f"ERROR INTERNAL: Illegal path {parts}")
            return self

        sub = self.add_subdir(parts[n])
        if sub is None:
            return self
        return sub.insert_parts(parts, n + 1)

    def insert_son(self, relative_path):
        """
        insert in the appropriate son, and all the intermediates
        returns the last inserted, which will be the father of the file
        """
        pure = pathlib.PurePath(relative_path).parts
        return self.insert_parts(pure, 0)


# -------------------------------------------
# Class BirsyncFile
# -------------------------------------------


class BirsyncFile:
    def __init__(self, filename, local_dir, remote_dir,
                 inode=None, mode=None, uname=None, user=None, size=None, mdate=None, group=None):
        self.relative_filename = filename
        pure = pathlib.PurePath(filename)
        self.relative_dir = pure.parent.name
        self.inode = inode
        self.mode = mode
        self.uname = uname
        self.user = user
        self.size = size
        self.mdate = mdate
        self.group = group
        self.src_transition: Trans = Transition.Absent
        self.dst_transition: Trans = Transition.Absent
        self.src_dryrun: RSChoice = RsyncChoice.RsyncSkip
        self.dst_dryrun: RSChoice = RsyncChoice.RsyncSkip
        self.decision: Deci = Decision.RsyncError
        self.previous_relative_filename = None  # in the Previous stage, previous_relative_path had the same inode
        self.new_relative_filename = None  # in the Current stage, new_relative_path has the same inode
        self.local_dir_root = local_dir
        self.remote_dir_root = remote_dir
        self.src_changed = '.'
        self.dst_changed = '.'

    def __str__(self):
        return self.relative_filename

    def __repr__(self):
        return "file[" + self.relative_filename + ',' + self.inode + ']'

    def changed(self, new_descriptor, origin):
        """
        returns true iff the descriptors differ
        uses the results of rsync Dry-run as a hint for Changed
        """

        attribute = "src_changed" if origin == "src" else "dst_changed"
        if self.inode != new_descriptor.inode:
            self.__setattr__(attribute, "i")
            return True
        if self.mdate != new_descriptor.mdate:
            self.__setattr__(attribute, "d")
            return True
        if self.relative_filename != new_descriptor.relative_filename:
            self.__setattr__(attribute, "n")
            return True
        if self.size != new_descriptor.size:
            self.__setattr__(attribute, "s")
            return True

        # We do not need to investigate change of uname etc, because this is done by rsync.
        # if self.uname != new_descriptor.uname or \
        #         self.user != new_descriptor.user or \
        #         self.group != new_descriptor.group:
        #     return True

        if origin == "src":
            if self.src_dryrun == RsyncChoice.RsyncChanged:
                self.__setattr__(attribute, "o")
                return True
            if new_descriptor.src_dryrun == RsyncChoice.RsyncChanged:
                self.__setattr__(attribute, "O")
                return True

        if origin != "src":
            if self.dst_dryrun == RsyncChoice.RsyncChanged:
                self.__setattr__(attribute, "p")
                return True
            if new_descriptor.dst_dryrun == RsyncChoice.RsyncChanged:
                self.__setattr__(attribute, "P")
                return True
        return False

    def set_rsync_src_dryrun(self, reason):
        if self.src_dryrun == RsyncChoice.RsyncSkip:
            self.src_dryrun = reason
        else:
            Printer.print(f"ERROR: set_rsync_src_state({self.relative_filename}) : {reason.str} vs " +
                          f" '{self.src_dryrun.str}'", Printer.Debug)

    def set_rsync_dst_dryrun(self, reason):
        if reason == RsyncChoice.RsyncSkip:
            # means that the we are re-assessing the file
            reason = self.dst_dryrun

        if self.dst_dryrun == RsyncChoice.RsyncSkip:
            self.dst_dryrun = reason

    def set_transition(self, updates, reason, origin, src_path, dst_path):
        """
        Pushes DESCR in the UPDATES hash of files to be updated, for REASON
        """
        Printer.print("Push (" + self.relative_filename + ') : ' + origin + ':' + reason.str, Printer.Deep)
        if self.relative_filename in updates:
            if origin == "local":
                updates[self.relative_filename].src_transition = reason
            else:
                updates[self.relative_filename].dst_transition = reason
            if self.previous_relative_filename is not None:
                updates[self.relative_filename].previous_relative_filename = self.previous_relative_filename
            if self.new_relative_filename is not None:
                updates[self.relative_filename].new_relative_filename = self.new_relative_filename
        else:
            if origin == "local":
                self.src_transition = reason
                self.dst_transition = Transition.Absent
            else:
                self.dst_transition = reason
                self.src_transition = Transition.Absent

            updates[self.relative_filename] = self
            updates[self.relative_filename].local_dir = src_path
            updates[self.relative_filename].remote_dir = dst_path

    def compute_decision(self, father):
        # father is the Birsync
        #
        # The potential Decision for each file, with their 1 char display:
        #     Skip     = rsync will not change this file on either ends, because unmodified
        #     Src2Dst  > rsync will copy from source to destination
        #     Dst2Src  < rsync will copy from source to destination
        #     Conflict C the Source file will be renamed locally, then files will be synchronized in both directions
        #     RmSrc    D Delete on the source, because was deleted on the destination
        #     RmDst    d delete on the destination, because was deleted on the source
        #     MvSrc    M move (i.e. change name) on the source, because was renamed on destination
        #     MvDst    m Move (i.e. change name) on the destination, because was renamed on source
        #     Null     # No decision is necessary, rsync will not manage this file
        #     Error    ! is an internal error, it should never happen
        #
        #   Dest:  CUMDVRA      Source
        matrix = ("CCC>>C>",  # Created
                  "C=<D=C>",  # Unmodified
                  "C>C>>CC",  # Modified
                  "<d<##<#",  # Deleted
                  "<=<##<#",  # V Moved OUT
                  "CCC>>Cm",  # Renamed IN
                  "<<<##M#")  # Absent

        updates_hash = father.updates  # Hash[filename] = BirsyncFile that are updated
        if self.src_transition == Transition.Created and \
                self.dst_transition == Transition.Created and \
                self.src_dryrun == RsyncChoice.RsyncSkip and \
                self.dst_dryrun == RsyncChoice.RsyncSkip:
            self.decision = Decision.Skip
            # DONE: Manage the CC case when dryrun has decided to not synchronize
            # presumably the file has been transmitted by other means
            # or the previous state was NOT computed OK
            return

        if not isinstance(self.src_transition, Trans):
            Printer.print(f"ERROR INTERN: set_rsync_src_state: src {self.relative_filename}:{self.src_transition.str}")
        if not isinstance(self.dst_transition, Trans):
            Printer.print(f"ERROR INTERN: set_rsync_dst_state: dst {self.relative_filename}:{self.dst_transition.str}")

        index_src = self.src_transition.value
        index_dst = self.dst_transition.value
        res = matrix[index_src][index_dst]

        self.decision = Decision.from_char(res)

        # Patching A+R = M
        if self.src_transition == Transition.Absent and \
                self.dst_transition == Transition.Renamed:
            old = self.previous_relative_filename
            if old in updates_hash:
                old_file = updates_hash[old]
                if old_file.src_transition == Transition.Unmodified and \
                        old_file.dst_transition == Transition.Moved and \
                        father.unchanged_file("dst", old, self.relative_filename):
                    Printer.print(f"decision: A+R=M (mv src) confirmed {self.previous_relative_filename}")
                    return
            # we will not be able to move from old
            Printer.print(f"decision: A+R=M (mv src) NOT confirmed: < {self.previous_relative_filename}")
            self.decision = Decision.Dst2Src

        # Patching R+A = m
        if self.src_transition == Transition.Renamed and \
                self.dst_transition == Transition.Absent:
            old = self.previous_relative_filename
            if old in updates_hash:
                old_file = updates_hash[old]
                if old_file.src_transition == Transition.Moved and \
                        old_file.dst_transition == Transition.Unmodified and \
                        father.unchanged_file("src", old, self.relative_filename):
                    Printer.print(f"decision: R+A=m  confirmed {self.previous_relative_filename}")
                    return
                # we will not be able to move from old
            Printer.print(f"decision: R+A=m (mv dst) NOT confirmed: > {self.previous_relative_filename}")
            self.decision = Decision.Src2Dst

        # Patching V+U = #
        if self.src_transition == Transition.Moved and \
                self.dst_transition == Transition.Unmodified:
            new_name = self.new_relative_filename
            if new_name in updates_hash:
                new_file = updates_hash[new_name]
                if new_file.src_transition == Transition.Renamed and \
                        new_file.dst_transition == Transition.Absent and \
                        father.unchanged_file("src", self.relative_filename, new_name):
                    Printer.print(f"decision: V+U=# (nothing) confirmed {self.new_relative_filename}")
                    return
            # we will not be able to move from old
            Printer.print(f"decision: V+U=d (RmDst)            {self.new_relative_filename}")
            self.decision = Decision.RmDst

        # Patching U+V = #
        if self.src_transition == Transition.Unmodified and \
                self.dst_transition == Transition.Moved:
            new_name = self.new_relative_filename
            if new_name in updates_hash:
                new_file = updates_hash[new_name]
                if new_file.src_transition == Transition.Absent and \
                        new_file.dst_transition == Transition.Renamed and \
                        father.unchanged_file("dst", self.relative_filename, new_name):
                    Printer.print(f"decision: U+V=# (nothing) confirmed {self.new_relative_filename}")
                    return
            # we will not be able to move from old
            Printer.print(f"decision: U+V=D (RmSrc)            {self.new_relative_filename}")
            self.decision = Decision.RmSrc

    def debug(self, msg='', level=Printer.Debug):
        old = '' if self.previous_relative_filename is None else " <-- '" + self.previous_relative_filename + "' "
        new = '' if self.new_relative_filename is None else " --> '" + self.new_relative_filename + "' "
        Printer.print(msg + self.src_transition.char +
                      self.dst_transition.char +
                      self.src_dryrun.char +
                      self.src_changed +
                      self.dst_dryrun.char +
                      self.dst_changed +
                      self.decision.char +
                      "  '" + self.relative_filename + "' " +
                      old + new, level)

    def do_error(self, msg="ERROR:"):
        self.debug(f"{msg}", level=PrintDebug.Normal)

    def do_delete_source(self, source_host, list_only):
        path = self.local_dir_root + '/' + self.relative_filename
        Printer.print("Transition delete local {path} : " +
                      str(self.src_transition) + ' : ' + str(self.dst_transition), Printer.Deep)
        source_host.remove_file(path, list_only)

    def do_delete_destination(self, destination_host, list_only):
        path = self.remote_dir_root + '/' + self.relative_filename
        Printer.print("Transition delete remote " + self.relative_filename +
                      ' : ' + self.src_dryrun.str + ' : ' + self.dst_dryrun.str, Printer.Debug)
        # cmdline = f'if [ -e {path} ]; then rm -f {path} ; else echo "file {path} not found" ; fi'
        destination_host.remove_file(path, list_only)

    def do_move_source(self, source_host, list_only):
        path1 = self.local_dir_root + '/' + self.previous_relative_filename
        path2 = self.local_dir_root + '/' + self.relative_filename
        source_host.move_file(path1, path2, list_only)

    def do_move_destination(self, destination_host, list_only):
        path1 = self.remote_dir_root + '/' + self.previous_relative_filename
        path2 = self.remote_dir_root + '/' + self.relative_filename
        destination_host.move_file(path1, path2, list_only)

    def do_conflict(self, local_host: Host, list_only):
        Printer.print(f"Transition conflict {self.relative_filename} : " +
                      f" dryrun {self.src_dryrun.str}:{self.dst_dryrun.str}" +
                      f" status {self.src_transition.str}:{self.dst_transition.str}", Printer.Debug)
        path = self.local_dir_root + '/' + self.relative_filename

        pp = pathlib.PurePath(self.relative_filename)

        new_filename = str(pp.parent) + '/' + pp.stem + '-conflict-' + local_host.postfix + pp.suffix
        new_path = self.local_dir_root + '/' + new_filename

        if self.relative_filename == local_host.ignore_filename:
            local_host.ignore_filename = pp.stem + '-conflict-' + local_host.postfix + pp.suffix
            # We have to move the local_host.ignore_filename to its new location
            # CAVEAT: We know that ignore_filename is at the ROOT directory, so pp.parent == .
            # so the proposed value is correct, and avoids to have ./ at the start of the filename,
            # which makes filename comparison more difficult

        local_host.move_file(path, new_path, list_only)
        #  Dans le cas birsync remote --> local
        #  source_host est un SshHost
        return new_filename

    def execute_decisions(self, src_host: Host, dst_host: Host, list_only):
        verbose_level = Printer.Debug if list_only else Printer.Normal
        # verbose_level for the actions that are omitted in list_only

        if self.decision == Decision.Skip:
            self.debug("decision: ", level=verbose_level)
            return
        if self.decision == Decision.Src2Dst:
            self.debug("decision: ", level=Printer.Normal)
            src_host.src_to_dst_files += self.relative_filename + "\n"
            return

        if self.decision == Decision.Dst2Src:
            self.debug("decision: ", level=Printer.Normal)
            dst_host.dst_to_src_files += self.relative_filename + "\n"
            return

        if self.decision == Decision.Conflict:
            self.debug("decision: ", level=Printer.Normal)
            new_local = self.do_conflict(src_host, list_only)
            src_host.src_to_dst_files += new_local + "\n"
            dst_host.dst_to_src_files += self.relative_filename + "\n"
            return

        if self.decision == Decision.RmSrc:
            self.debug("decision: ", level=Printer.Normal)
            self.do_delete_source(src_host, list_only)
            # No synchronization required: Delete did the job
            return

        if self.decision == Decision.RmDst:
            self.debug("decision: ", level=Printer.Normal)
            self.do_delete_destination(dst_host, list_only)
            # No synchronization required: Delete did the job
            return

        if self.decision == Decision.MvSrc:
            self.debug("decision: ", level=Printer.Normal)
            self.do_move_source(src_host, list_only)
            # No synchronization required: Move did the job
            return

        if self.decision == Decision.MvDst:
            self.debug("decision: ", level=Printer.Normal)
            self.do_move_destination(dst_host, list_only)
            # No synchronization required: Move did the job
            return

        if self.decision == Decision.Null:
            self.debug("decision: ", level=verbose_level)
            return
        # else: ERROR
        self.do_error("UNKNOWN DECISION :")


#
# ------------ end of Class BirsyncFile ------------
#


# ---------------------------------------------------------------
# Birsync
# ---------------------------------------------------------------


class Birsync:
    def __init__(self,
                 source_user=None, source_hostname=None, source_port=None, source_dir=None,
                 dest_user=None, dest_hostname=None, dest_port=None, dest_dir=None,
                 rsync_options=None,
                 bash=None,
                 verbose=PrintDebug.Normal,
                 ssh=None,
                 rsync=None,
                 postfix=None,
                 hostname=None,
                 list_only=False,
                 parse_only=False,
                 self_test=False):

        local_hostname = hostname or platform.node()

        if self_test:
            stamp = datetime.datetime.today().strftime('%Y-%m-%d_T%H-%M-%S')
            temp_dir = None
            if source_dir is None:
                source_dir = temp_dir = tempfile.mkdtemp("_birsync")
            source_dir = Host.local_filename(source_dir + '/src_' + stamp)

            if dest_dir is None:
                dest_dir = temp_dir or tempfile.mkdtemp("_birsync")
            dest_dir = Host.local_filename(dest_dir + '/dst_' + stamp)

        self.source_host = Host.create(self, source_dir, user=source_user, hostname=source_hostname,
                                       port=source_port, bash=bash, ssh=ssh, rsync=rsync, postfix=postfix,
                                       local_hostname=local_hostname)
        self.dest_host = Host.create(self, dest_dir, user=dest_user, hostname=dest_hostname,
                                     port=dest_port, bash=bash, ssh=ssh, rsync=rsync, postfix=postfix,
                                     local_hostname=local_hostname)
        self.local_host = self.source_host if self.source_host.is_local() else self.dest_host
        # local_host is where rsync is executed

        self.rsync_options = rsync_options or ["-vrPEptuC"]

        src_d = f"{self.source_host.hostname_for_hash}_{self.source_host.hash_stamp}"
        dst_d = f"{self.dest_host.hostname_for_hash}_{self.dest_host.hash_stamp}"
        self.remote_file = f".rsync_{src_d}_to_{dst_d}"
        self.local_file = f".rsync_{dst_d}_to_{src_d}"
        self.list_only = list_only
        self.verbose = verbose

        self.ignore_files = []         # a list of regular expressions for files to be ignored
        self.ignore_dirs = []
        self.previous_dst_files = {}   # Hash[relative filename] = BirsyncFile
        self.previous_dst_inodes = {}
        self.current_dst_files = {}
        self.current_dst_inodes = {}
        self.previous_src_files = {}
        self.previous_src_inodes = {}
        self.current_src_files = {}
        self.current_src_inodes = {}
        self.updates = {}              # idem. The files that must be updated according to rsync

        Printer.set_level(self.verbose)
        if parse_only:
            verbose_str = "normal"
            if verbose == PrintDebug.Silent:
                verbose_str = "Silent"
            elif verbose == PrintDebug.Quiet:
                verbose_str = "Quiet"
            elif verbose == PrintDebug.Verbose:
                verbose_str = "Verbose"
            elif verbose == PrintDebug.Debug:
                verbose_str = "Debug"
            elif verbose == PrintDebug.Deep:
                verbose_str = "Deep"

            Printer.print(f"-------------- Options according to command-line: ")
            Printer.print(f"source user:  '{self.source_host.user}' change with --user ")
            Printer.print(f"source host:  '{self.source_host.hostname}' change with --source-host ")
            Printer.print(f"source port:  '{self.source_host.port}' change with --port ")
            Printer.print(f"source dir:   '{self.source_host.directory}' ")
            Printer.print(f"source hash:  '{self.source_host.hash_stamp}' ")
            Printer.print(f"")
            Printer.print(f"remote user:  '{self.dest_host.user}' change with --user ")
            Printer.print(f"remote host:  '{self.dest_host.hostname}' change with --remote-host ")
            Printer.print(f"remote port:  '{self.dest_host.port}' change with --port ")
            Printer.print(f"remote dir:   '{self.dest_host.directory}' ")
            Printer.print(f"remote hash:  '{self.dest_host.hash_stamp}' ")
            Printer.print(f"")

            Printer.print(f"rsync_options:'{self.rsync_options}' change with " +
                          "--rsync-(long/short/no-short)-option ")
            Printer.print(f"verbose:      '{verbose_str}' change with --verbose(-silent/quiet/debug/deep)")
            Printer.print(f"list_only:    '{list_only}'")
            Printer.print(f"parse_only:   '{parse_only}'")
            Printer.print(f"self_test:    '{self_test}'")
            Printer.print(f"")
            Printer.print(f"host name     '{hostname}'")
            Printer.print(f"Host OS       '{platform.system()}'")
            Printer.print(f"bash:         '{self.local_host.bash}'")
            Printer.print(f"ssh:          '{self.local_host.ssh}'")
            Printer.print(f"rsync:        '{self.local_host.rsync_exec}'")
            Printer.print(f"stamp         '{self.local_host.postfix}'")
            exit(0)

    def unchanged_file(self, loc, previous_name, current_name):
        if loc == "src":
            previous_file = self.previous_src_files[previous_name]
            current_file = self.current_src_files[current_name]
        else:
            previous_file = self.previous_dst_files[previous_name]
            current_file = self.current_dst_files[current_name]

        if previous_file.mdate == current_file.mdate and previous_file.size == current_file.size:
            return True
        return False

    def parse_ls_output(self, val, src_path, dst_path, file_hash, inode_hash, current_dir='', ignore=True):
        """
        val = the output of ls -algir, recursive or not recursive
        current_dir = ''  # the path of the directory being analysed relative to root_path, with trailing / if not ''
        ignore = True, in this case, files and directores that should be ignored are ignored.
        ignore = False. Used only when testing, to know the reality of files and directories

        What this function does: Parses val
        src_path and dst_path are used to build the BirsyncFile
        modifies
            inode_hash = hash[inode] BirsyncFile
            file_hash = hash[filename] BirsyncFile

        returns
            list_of_directories as parsed

        if ls is recursive, the directory path is listed, then the files and directories inside the directory
        otherwise, the directory path is not listed, only the files and directories
        example of lines:
            '36873221949499401 -rw-rw-rw- 0 0 59957 2020-02-02 21:30:07 +0100 synchro.py'
        """
        all_lines = re.split(r'\n|\\n', val)
        # if all_lines[0][0:2] == "b'":
        #     all_lines[0] = all_lines[0][2:]

        root_dir = BirsyncDir('', None)
        root_path = None  # the absolute complete Path of the root directory, always '' or end with /
        line_nb = 0
        for line in all_lines:
            line_nb += 1
            m = re.match(
                r' *(\d+) ([drwxst\-+]+) +(\d+) +((\S+) +)?(\S+) +(\d+) ' +
                r'+(\d\d\d\d-\d\d-\d\d \d\d:\d\d:\d\d\.?\d* [+\-\d]+) +(.+)',
                line)
            if m is not None:
                filename = m.group(9)
                if re.match(r'^(\.|\.\.|\.rsync_.*)$', filename):
                    continue
                if re.match(r'^\.bysync_(to|from)_file$', filename):
                    continue

                filename = Host.unquote_filename(filename)
                relative_filename = current_dir + filename

                if re.match(r'^d', m.group(2)):
                    # mode d = directory
                    pseudo = relative_filename + '/_foo_lee_bah_.txt'
                    if filename != '.' and filename != '..' and \
                            (ignore is False or not self.is_ignored_path(pseudo)):
                        # foo = a trick to reuse insert_son
                        root_dir.insert_son(pseudo)
                    continue

                if re.match("([a-zA-Z]):([/\\\\]*)", filename):
                    # Sometimes, errors on a synology NAS
                    continue

                if ignore and self.is_ignored_path(relative_filename):
                    continue

                descriptor = BirsyncFile(relative_filename,
                                         src_path, dst_path,
                                         inode=m.group(1),
                                         mode=m.group(2),
                                         uname=m.group(3),
                                         user=m.group(4),
                                         group=m.group(6),
                                         size=m.group(7),
                                         mdate=dateutil.parser.parse(m.group(8)))
                file_hash[relative_filename] = descriptor
                inode_hash[descriptor.inode] = descriptor
            else:
                m = re.match(r'^#.*$', line)
                if m is not None:
                    continue
                m = re.match(r'^total \d*$', line)
                if m is not None:
                    continue
                m = re.match(r'^(.*):$', line)
                if m is None:
                    continue
                # this is a directory from a ls -R recursive
                # all subsequent files will belong to this directory
                new_path = m.group(1)
                if root_path is None:
                    # the fist path encountered is the absolute path of the root directory
                    root_path = new_path + '/'
                else:
                    length = len(root_path)
                    found = new_path.find(root_path)
                    if found == 0:
                        # current_dir is a path relative to root_path, so we remove the root_path part
                        current_dir = new_path[length:] + '/'
                        # 2 test cases:
                        #   in Unitary test, with a ls -lR, i.e. recursive call to ls
                        #   with complete tests, where parse is called directory per directory

        return root_dir

    #  -i, --itemize-changes
    # The general format is like the string YXcstpoguax
    #
    # Y, 1st char:
    #  f for a file, a d for a directory, an L for a symlink, a D for a device,
    #  and a S for  a  special file (e.g. named sockets and fifos)
    # X 2nd char:
    # < means that a file is being transferred to the remote host (sent)
    # > means that a file is being transferred to the local host (received)
    # c means that a local change/creation is occurring for the item (such as the creation of a directory
    #   or the changing of a sym‐link, etc.)
    # . means that the item is not being updated
    # * means that the rest of the itemized-output area contains a message (e.g. "deleting")
    #
    # other letters are reason for change
    # . is no change
    # + is file created
    # space is identical   Pas clair, quelle différence avec . ?
    # c if checksum change
    # s if size change
    # t or T if time change
    # p if permission change
    # o if owner change
    # g if group change
    # u undefined
    # a ACL change
    # x extended attributes changed

    def parse_rsync_dryrun(self, val, origin, src_path, dst_path, files_hash, root_dir: BirsyncDir = None):
        """
        Parse the output of rsync --dry-run -riu
        val is the string to be parsed
        origin = 'src' if dryrun was run on the src
        files_hash is a hash[filename] = BirsyncFile
        dirs_hash is a hash[dirname] = BirsyncDir
        manages directory structure, relative to the root
        returns the BirsyncDir of the root
        populates the file_hash that describes all files in both locations
        """

        all_lines = re.split(r'\n|\\n', val)
        if all_lines[0][0:2] == "b'":
            all_lines[0] = all_lines[0][2:]

        if root_dir is None:
            root_dir = BirsyncDir('', None)
        files_to_manage = {}
        for line in all_lines:
            m = re.match(r'^(.)f(\S)\S+ (.*)$', line)
            if m is None:
                continue
            # We do a first pass to look for .birsync_ignore files
            # Here, we manage only files that rsync decides to update

            mode = m.group(1)
            status = Transition.Created if m.group(2) == "+" else Transition.Modified
            filename = m.group(3)

            p = re.match(r"^'(.*)'$", filename)
            if p:
                filename = p.group(1)
            p = re.match(r'^"(.*)"$', filename)
            if p:
                filename = p.group(1)

            if re.match(r'.*(|/|\\)\.rsync_[^/\\]*$', filename):
                # do not manage those files
                continue

            if mode == '>' or mode == '<':
                files_to_manage[filename] = status

        for filename in files_to_manage:
            root_dir.insert_son(filename)
            if self.is_ignored_path(filename):
                continue
            if filename not in files_hash:
                files_hash[filename] = BirsyncFile(filename, src_path, dst_path)

            descriptor = files_hash[filename]

            status = files_to_manage[filename]
            if origin == 'src':
                descriptor.set_rsync_src_dryrun(status)
            else:
                descriptor.set_rsync_dst_dryrun(status)

        Printer.print(f"parse_rsync_dryrun ({origin})", Printer.Debug)
        for filename in files_hash:
            files_hash[filename].debug()

        return root_dir

    def parse_ignore(self, src_path):

        val = self.source_host.read_file(src_path + '/' + self.source_host.ignore_filename, print_error=False)

        all_lines = re.split(r'\n|\\n', val)

        for line in all_lines:
            line = line.strip()
            # DONE: '#' can be escaped in the .birsync-ignore file
            if len(line) > 1 and line[0] == '\\' and line[1] == '#':
                #  we want to skip a directory or file starting with #, which is possible
                line = line[1:]
            else:
                m = re.match(r'^([^#]*)#.*$', line)
                if m:
                    line = m.group(1)
            if line == '':
                continue
            pp = re.match(r"(.*)[/\\]\*?$", line)
            if pp:
                self.ignore_dirs.append('^' + pp.group(1) + '$')
            else:
                self.ignore_files.append('^' + line + '$')

    def is_ignored_file(self, filename):
        """returns TRUE iff filename is ignored in this directory"""
        for pattern in self.ignore_files:
            if re.match(pattern, filename):
                return True
        return False

    def is_ignored_dir(self, dir_name):
        """returns TRUE iff filename is ignored in this directory"""
        for pattern in self.ignore_dirs:
            if re.match(pattern, dir_name):
                return True
        return False

    def is_ignored_full_dir(self, relative_path):
        pure = pathlib.PurePath(relative_path)
        pp = pure.parts
        for my_dir in pp[0:-1]:
            if self.is_ignored_dir(my_dir):
                return True
        return False

    def is_ignored_path(self, relative_path):
        pure = pathlib.PurePath(relative_path)
        return self.is_ignored_full_dir(relative_path) or self.is_ignored_file(pure.name)

    #
    # ---------------- rsync: execute the protocol --------------------------
    #

    def rsync(self, from_path, to_path, from_host, to_host, dry_run=False, files=None):
        """synchronizes, with rsync, between src and dst
        from_xxx can be either local or remote, source or destination
        """

        cmd_line = [self.local_host.rsync_exec] + self.rsync_options + ['-e', self.local_host.ssh]
        if dry_run and "--dry-run" not in cmd_line:
            cmd_line += ["-i", "--dry-run"]

        if files is not None:
            cmd_line += ["--files-from", files]

        if from_host.port is not None:
            cmd_line += ["--port", from_host.port]
            if to_host.port is not None:
                Printer.print(f"ERROR: Port for local and remote")
                exit(-1)
        elif to_host.port is not None:
            cmd_line += ["--port", to_host.port]

        cmd_line += [from_host.quoted_linux_filename(from_path + '/'),  # quoted
                     to_host.quoted_linux_filename(to_path)]  # quoted
        if self.list_only and not dry_run:
            Printer.print(f"birsync WOULD synchronize: [{cmd_line}]", Printer.Verbose)
            return ""
        ret_val = self.local_host.execute_linux_cmd(cmd_line)  # always local_host that executes rsync

        if self.list_only:
            res = str(ret_val)
            res = re.sub(r'\\n', '\n', res)
            res = re.sub(r'^b\'', "", res)
            res = re.sub(r'\'$', "", res)
            Printer.print(res, Printer.Verbose)
        return ret_val

    # -----------------------------------------------------------
    # get_dryrun_status
    # No need to have a recursive implementation per directory

    def get_dryrun_status(self, src_dir, dst_dir, src_host, dst_host, src_path, dst_path, files_hash):
        """
        files_hash is a hash[filename] = BirsyncFile
        executes rsync dryrun between src and dst, and returns the description of files
        :return: root_src
        """
        root_src = BirsyncDir('', None)
        res = self.rsync(src_path, dst_path, src_host, dst_host,  dry_run=True)
        root_src = self.parse_rsync_dryrun(res, 'src', src_dir, dst_dir, files_hash, root_src)

        res = self.rsync(dst_path, src_path, dst_host, src_host,  dry_run=True)
        root_src = self.parse_rsync_dryrun(res, 'dst', src_dir, dst_dir, files_hash, root_src)

        return root_src

    # -----------------------------------------------------------
    # Process file lists
    # -----------------------------------------------------------

    def process_file_lists(self, src_dir, dst_dir, src_path, dst_path):
        """
        src_dir:  absolute path to be synchronized on host
        dst_dir:  absolute path to be synchronized on host
        src_path: src_dir completed with the rsync prefix
        dst_path: dst_dir completed with the rsync prefix

        Process file lists from source and destination directories
        Which one is local and which is remote is unknown
        a file has been moved if the same inode has a different filename
        a file has been removed if the same inode does not exist anymore

        returns root_dir, the descriptor of the root directory
        """

        self.previous_dst_files = {}
        self.previous_dst_inodes = {}
        self.current_dst_files = {}
        self.current_dst_inodes = {}
        self.updates = {}

        root_dir = self.get_dryrun_status(src_dir, dst_dir, self.source_host, self.dest_host,
                                          src_path, dst_path, self.updates)

        self.dest_host.get_previous_iterative(dst_dir, self.remote_file, src_dir, dst_dir, root_dir.tree,
                                              self.previous_dst_files, self.previous_dst_inodes)
        self.dest_host.dir_descriptor = self.dest_host.get_current(dst_dir, src_dir, dst_dir, self.current_dst_files,
                                                                   self.current_dst_inodes)

        # Build the status for each file, local and remote.

        for file in self.previous_dst_files.keys():
            if file not in self.updates:
                Printer.print(f"Optimizing file '{file}' seen unchanged by dryrun")
                continue

            descriptor = self.previous_dst_files[file]
            inode = descriptor.inode
            if file not in self.current_dst_files:
                if inode in self.current_dst_inodes:
                    descriptor.new_relative_filename = self.current_dst_inodes[inode].relative_filename
                    descriptor.set_transition(self.updates, Transition.Moved, 'remote', src_dir, dst_dir)
                else:
                    descriptor.set_transition(self.updates, Transition.Deleted, 'remote', src_dir, dst_dir)
            else:
                new_descriptor = self.current_dst_files[file]
                if descriptor.changed(new_descriptor, 'dst'):
                    descriptor.set_transition(self.updates, Transition.Modified, 'remote', src_dir, dst_dir)
                else:
                    descriptor.set_transition(self.updates, Transition.Unmodified, 'remote', src_dir, dst_dir)

        for file in self.current_dst_files.keys():
            if file not in self.updates:
                if file not in self.previous_dst_files:
                    # otherwise, print already done.
                    Printer.print(f"Optimizing file '{file}' seen unchanged by dryrun")
                continue

            descriptor = self.current_dst_files[file]
            inode = descriptor.inode
            if file not in self.previous_dst_files:
                if inode in self.previous_dst_inodes:
                    descriptor.previous_relative_filename = self.previous_dst_inodes[inode].relative_filename
                    descriptor.set_transition(self.updates, Transition.Renamed, 'remote', src_dir, dst_dir)
                else:
                    descriptor.set_transition(self.updates, Transition.Created, 'remote', src_dir, dst_dir)

            # else, file was in previous_remote, and has been managed

        #
        # manage local files
        #

        self.previous_src_files = {}
        self.previous_src_inodes = {}
        self.current_src_files = {}
        self.current_src_inodes = {}

        self.source_host.dir_descriptor = \
            self.source_host.get_current(src_dir, src_dir, dst_dir, self.current_src_files, self.current_src_inodes)
        self.source_host.get_previous_iterative(src_dir, self.local_file, src_dir, dst_dir, root_dir.tree,
                                                self.previous_src_files, self.previous_src_inodes)

        # process the src files
        #
        for file in self.previous_src_files.keys():
            if file not in self.updates:
                if file not in self.previous_dst_files and file not in self.current_dst_files:
                    Printer.print(f"Optimizing file '{file}' seen unchanged by dryrun")
                continue

            descriptor = self.previous_src_files[file]
            inode = descriptor.inode
            if file not in self.current_src_files:
                if inode in self.current_src_inodes:
                    descriptor.new_relative_filename = self.current_src_inodes[inode].relative_filename
                    descriptor.set_transition(self.updates, Transition.Moved, 'local', src_dir, dst_dir)
                else:
                    descriptor.set_transition(self.updates, Transition.Deleted, 'local', src_dir, dst_dir)
            else:
                new_descriptor = self.current_src_files[file]
                if descriptor.changed(new_descriptor, 'src'):
                    descriptor.set_transition(self.updates, Transition.Modified, 'local', src_dir, dst_dir)
                else:
                    descriptor.set_transition(self.updates, Transition.Unmodified, 'local', src_dir, dst_dir)

        for file in self.current_src_files.keys():
            if file not in self.updates:
                if file not in self.previous_dst_files and \
                        file not in self.current_dst_files and \
                        file not in self.previous_src_files:
                    Printer.print(f"Optimizing file '{file}' seen unchanged by dryrun")
                continue

            descriptor = self.current_src_files[file]
            inode = descriptor.inode
            if file not in self.previous_src_files:
                if inode in self.previous_src_inodes:
                    descriptor.previous_relative_filename = self.previous_src_inodes[inode].relative_filename
                    descriptor.set_transition(self.updates, Transition.Renamed, 'local', src_dir, dst_dir)
                else:
                    descriptor.set_transition(self.updates, Transition.Created, 'local', src_dir, dst_dir)
            # else, file was in previous_remote, and has been managed

        # use the matrix for decision
        for file in self.updates.keys():
            descriptor = self.updates[file]
            descriptor.compute_decision(self)
            descriptor.execute_decisions(self.source_host, self.dest_host, self.list_only)

        return root_dir

    def process_dir_lists(self, src_dir, dst_dir):
        # if a dir is on one side and not on the other, let's try to remove it.
        # we need to do it top to bottom, so that the sub dirs may have been removed.
        src_dir_list = []
        for d in self.source_host.dir_descriptor.tree:
            if d not in self.dest_host.dir_descriptor.tree:
                src_dir_list.append(d)

        src_dir_list = sorted(src_dir_list, key=lambda ll: len(ll), reverse=True)
        for d in src_dir_list:
            res = "succeed" if self.source_host.remove_dir(src_dir + '/' + d, PrintDebug.Normal, empty_only=True) \
                else "skipped : not empty"
            Printer.print(f"Directory optimize remove: source '{d}' {res}")

        dst_dir_list = []
        for d in self.dest_host.dir_descriptor.tree:
            if d not in self.source_host.dir_descriptor.tree:
                dst_dir_list.append(d)

        dst_dir_list = sorted(dst_dir_list, key=lambda ll: len(ll), reverse=True)
        for d in dst_dir_list:
            res = "succeed" if self.dest_host.remove_dir(dst_dir + '/' + d, PrintDebug.Normal, empty_only=True) \
                else "skipped : not empty"
            Printer.print(f"Directory optimize remove: dest '{d}' {res}")

    # -------------------------------------------------------
    # Synchronize
    # -------------------------------------------------------
    #

    def synchronize(self, src_dir, dst_dir, src_host: Host, dst_host: Host, src_file, dst_file):
        """
        Performs the synchronization
        :return: None
        """
        source_path = src_host.rsync_path + src_dir
        destination_path = dst_host.rsync_path + dst_dir

        src_host.src_to_dst_files = ""
        src_host.dst_to_src_files = ""

        dst_host.src_to_dst_files = ""
        dst_host.dst_to_src_files = ""

        if not src_host.dir_exists(src_dir):
            Printer.print(f"ERROR: source directory '{src_dir}' does not exist")
            exit(-1)
        if not dst_host.dir_exists(dst_dir):
            Printer.print(f"ERROR: dest directory '{dst_dir}' does not exist")
            exit(-1)

        descriptor = f"# src host : '{src_host.fullname()}'\n"
        descriptor += f"# src dir  : '{src_dir}'\n"
        descriptor += f"# src file : '{src_file}'\n"

        Printer.print(f"Synchronize:  src host '{src_host.fullname()}'")
        Printer.print(f"Synchronize:  src dir  '{src_dir}' ")
        Printer.print(f"Synchronize:  src file '{src_file}'", Printer.Debug)

        descriptor += f"# dst host : '{dst_host.fullname()}'\n"
        descriptor += f"# dst dir  : '{dst_dir}'\n"
        descriptor += f"# dst file : '{dst_file}'\n"

        Printer.print(f"Synchronize:  dst host '{dst_host.fullname()}'")
        Printer.print(f"Synchronize:  dst dir  '{dst_dir}' ")
        Printer.print(f"Synchronize:  dst file '{dst_file}'", Printer.Debug)

        self.parse_ignore(src_dir)

        self.process_file_lists(src_dir, dst_dir, source_path, destination_path)

        if src_host.src_to_dst_files == "":
            Printer.print(f"Synchronize: rsync src > dst : No files -> no synchro")
        else:
            src_files = self.local_host.local_filename(self.local_host.directory + "/.birsync_to_files")
            src_files_lnx = self.local_host.quoted_linux_filename(src_files)
            with open(src_files, "w", encoding='utf8') as fh:
                print(src_host.src_to_dst_files, file=fh)
            Printer.print(f"Synchronize: rsync src > dst files list, local: '{src_files_lnx}'", Printer.Verbose)
            self.rsync(source_path, destination_path, src_host, dst_host, files=src_files_lnx)

        if dst_host.dst_to_src_files == "":
            Printer.print(f"Synchronize: rsync dst > src : No files -> no synchro")
        else:
            dst_files = Host.local_filename(self.local_host.directory + "/.birsync_from_files")
            dst_files_lnx = self.local_host.quoted_linux_filename(dst_files)
            with open(dst_files, "w", encoding='utf8') as fh:
                print(dst_host.dst_to_src_files, file=fh)
            Printer.print(f"Synchronize: rsync dst > src files list, local: '{dst_files_lnx}'", Printer.Verbose)
            self.rsync(destination_path, source_path, dst_host, src_host, files=dst_files_lnx)

        Printer.print(f"Synchronize: directory structure", Printer.Verbose)
        self.process_dir_lists(src_dir, dst_dir)

        if self.list_only:
            Printer.print("birsync WOULD build the source and destination indexes")
            return

        src_host.build_new_recursive(src_dir, src_file, msg="# source description\n" + descriptor)
        dst_host.build_new_recursive(dst_dir, dst_file, msg="# destination description\n" + descriptor)
        Printer.print(f"Synchronize: normal end", Printer.Verbose)

# ---------------------------------------------------------------
# unitary tests
# ---------------------------------------------------------------


def unitary_test(context: Birsync):
    inodes_hash = {}
    files_hash = {}
    res = context.parse_ls_output("""
total 16
149224 drwxrwxrwx+  2 users 4096 2020-01-04 18:49:14.277325301 +0100 .
264499 drwxrwxrwx+ 30 root  4096 2020-01-04 18:46:34.005760198 +0100 ..
149581 -rwxrwxrwx+  1 users 3062 2016-01-04 00:34:30.000000000 +0100 ToNAS.pl
  149224 drwxrwxrwx+  3 users 12288 2020-03-22 15:15:59.335038924 +0100 .
47712610 -rwxrwxrwx+  1 users     0 2020-03-22 15:15:59.335038924 +0100 .rsync_remote_dir_RENNLW7RDL4364
47322781 -rwxrwxrwx+ 1 users 59957 2020-02-02 21:30:07.662000000 +0100 synchro.py
11540474045575098 -rw-rw-rw- 0 0 88 2020-03-22 15:15:56.962000 +0100 .rsync_local_dir
 6473924464606103 drwxrwxrwx 0 0 0 2020-03-22 15:15:18 +0100 Test3
""", 'Src', 'Dst', files_hash, inodes_hash, ignore=False)
    Printer.print(files_hash)
    Printer.print(inodes_hash)
    Printer.print(res)

    inodes_hash = {}
    files_hash = {}
    res = context.parse_ls_output("""
/cygdrive/c/Users/battinif/Documents/PRIVATE-to-Fabien-BATTINI/Famille/TEST:
total 26
 49258120924678841 drwxrwxrwx+ 1 Domain Users    0 2020-01-20 19:49:29.756533100 +0100  .
  1970324837163614 d---rwx---+ 1 Domain Users    0 2020-01-07 17:19:24.381000400 +0100  ..
101330991616158540 -rw-r--r--  1 Domain Users 1147 2020-01-20 20:13:50.448000000 +0100  .rsync_local_dir
  9851624185293031 -rwx------  1 Domain Users  531 2018-08-14 11:30:16.000000000 +0200 'Config-Imprimante - Copy.txt'
  4503599627790478 -rwx------  1 Domain Users  531 2018-08-14 11:30:16.000000000 +0200  Config-Imprimante.txt
 40813871623115073 drwxrwxrwx+ 1 Domain Users    0 2020-01-04 18:49:14.000000000 +0100  TEST2
 10133099162004727 -rwxrwxrwx  1 Domain Users 3062 2016-01-04 00:34:30.000000000 +0100  ToNAS.pl

/cygdrive/c/Users/battinif/Documents/PRIVATE-to-Fabien-BATTINI/Famille/TEST/TEST2:
total 8
40813871623115073 drwxrwxrwx+ 1  EU+Group(513) Users    0 2020-01-04 18:49:14.000000000 +0100 .
49258120924678841 drwxrwxrwx+ 1  EU+Group(513) Users    0 2020-01-20 19:49:29.756533100 +0100 ..
 5910974511344892 -rwxrwxrwx  1  EU+Group(513) Users 3062 2016-01-04 00:34:30.000000000 +0100 ToNAS.pl
""", 'Src', 'Dst', files_hash, inodes_hash, ignore=False)
    Printer.print(files_hash)
    Printer.print(inodes_hash)
    Printer.print(res)

    files_hash = {}
    context.parse_rsync_dryrun("""
>f+++++++++ pattern_Deleted_Changed.txt
>f+++++++++ pattern_Deleted_New.txt
>f.sT...... pattern_Changed_Changed.txt
>f+++++++++ pattern_Moved_Changed.txt
>f.sT...... pattern_Unmodified_Changed.txt
>f+++++++++ subdir/pattern_Changed_Deleted.txt
>f.sT...... subdir/pattern_Changed_Changed.txt
    """, 'src', 'Src', 'Dst', files_hash)
    for descriptor in files_hash:
        Printer.print(descriptor)


# ---------------------------------------------------------------
# end to end test
# ---------------------------------------------------------------
# After comparing PREVIOUS and CURRENT status
# Each file can be in one of the following states:
# * New
# * Unmodified
# * Changed
# * Deleted
# * Moved
#


class Tester:
    def __init__(self, context: Birsync):
        self.context = context
        self.source_dir = context.source_host.directory
        self.destination_dir = context.dest_host.directory
        context.source_host.mkdir(context.source_host.directory)
        context.dest_host.mkdir(context.dest_host.directory)
        Printer.print(f"self-test '{self.source_dir}' --> '{self.destination_dir}'")

    @staticmethod
    def build_test_filename(path, local, remote, conflict=None):
        conf = "" if conflict is None else '-conflict-' + conflict

        if path != "":
            path = path + '/'
        return path + 'pattern_' + local + '_' + remote + conf + '.txt'

    def build_one_test_file(self, host, path, local, remote):
        """Builds 1 test file, on HOST, in PATH"""
        filename = self.build_test_filename(path, local, remote)
        host.build_one_file(filename, [f"Local  = {local}", f"remote = {remote}"])

    def change_one_test_file(self, host, path, local, remote, list_only):
        filename = self.build_test_filename(path, local, remote)
        host.change_file(filename, list_only)

    def remove_one_test_file(self, host, path, local, remote, list_only):
        filename = self.build_test_filename(path, local, remote)
        host.remove_file(filename, list_only)

    def move_one_test_file(self, host, path, local, remote, dest, list_only):
        filename = self.build_test_filename(path, local, remote)
        new_filename = self.build_test_filename(path, local, remote + "_" + dest)
        host.move_file(filename, new_filename, list_only)

    def build_initial_test_files(self, src_host, src_dir, dst_host, dst_dir, do_to_move):
        Printer.print("test: create '" + src_dir + "'", Printer.Debug)
        if do_to_move:
            for local in ('Unmodified', 'Changed', 'Deleted', 'Moved', 'ToMove'):
                for remote in ('Unmodified', 'Changed', 'Deleted', 'Moved', 'ToMove', 'ToMove_Renamed_rem'):
                    self.build_one_test_file(src_host, src_dir, local, remote)

            for remote in ('Unmodified', 'Changed', 'Deleted', 'Moved', 'ToMove', 'ToMove_Renamed_rem'):
                self.build_one_test_file(src_host, src_dir, 'ToMove', remote + '_Renamed_loc')
        else:
            for local in ('Unmodified', 'Changed', 'Deleted', 'Moved'):
                for remote in ('Unmodified', 'Changed', 'Deleted', 'Moved'):
                    self.build_one_test_file(src_host, src_dir, local, remote)

        src_host.build_one_file(src_dir + "/file with ' eacute and ç cedilla é")
        src_host.mkdir(src_dir + '/dir with é')
        src_host.build_one_file(src_dir + "/dir with é/typical-file.txt")
        src_host.build_one_file(src_dir + "/dir with é/non typical-file with ç.txt")
        src_host.mkdir(src_dir + '/dir with spaces')
        src_host.build_one_file(src_dir + "/dir with spaces/usual-file.txt")
        src_host.build_one_file(src_dir + "/dir with spaces/non usual-file.txt")
        src_host.build_one_file(src_dir + "/Thumbs.db")

        src_host.build_one_file(src_dir + "/.birsync-ignore",
                                ["Thumbs.db", "@eaDir/", "test2.txt", "subdir/", "to_be_ignored/"])

        src_host.mkdir(src_dir + '/@eaDir')
        src_host.build_one_file(src_dir + "/@eaDir/test.txt")
        src_host.mkdir(src_dir + '/dir with spaces/@eaDir')
        src_host.build_one_file(src_dir + "/dir with spaces/@eaDir/test.txt")

        src_host.mkdir(src_dir + "/to_be_removed")
        src_host.build_one_file(src_dir + "/to_be_removed/file1.txt")
        src_host.build_one_file(src_dir + "/to_be_removed/file2.txt")

        src_host.mkdir(src_dir + "/to_be_moved")
        src_host.build_one_file(src_dir + "/to_be_moved/file1.txt")
        src_host.build_one_file(src_dir + "/to_be_moved/file2.txt")
        src_host.mkdir(src_dir + "/to_be_moved/@eaDir")

        src_host.mkdir(src_dir + "/to_be_ignored")
        src_host.mkdir(src_dir + "/to_be_ignored/twice")

        dst_host.mkdir(dst_dir + "/subdir")
        dst_host.build_one_file(dst_dir + "/subdir/test.txt")
        dst_host.build_one_file(dst_dir + "/subdir/test2.txt")

        dst_host.mkdir(dst_dir + "/other_subdir")
        dst_host.build_one_file(dst_dir + "/other_subdir/test.txt")
        dst_host.build_one_file(dst_dir + "/other_subdir/test2.txt")

        dst_host.mkdir(dst_dir + "/to_be_removed2")
        dst_host.build_one_file(dst_dir + "/to_be_removed2/file1.txt")
        dst_host.build_one_file(dst_dir + "/to_be_removed2/file2.txt")

    #
    # if both source and destination are None, then all files are local and directories are created
    # otherwise, both dest-host, destination
    #

    def end_to_end_test(self, keep_files, list_only):
        do_to_move = False

        # No, we build files in the Source, either local or remote

        # Create the PREVIOUS state in SOURCE
        # the files that will be "new" in the CURRENT state are NOT created

        Printer.print(f"test: create '{self.source_dir}'", Printer.Verbose)
        self.build_initial_test_files(self.context.source_host, self.source_dir, self.context.dest_host,
                                      self.destination_dir, do_to_move)

        # copy all files between source and destination, and initialize the file lists.
        Printer.print(f"test: initialisation of '{self.destination_dir}' with '{self.source_dir}'", Printer.Verbose)
        self.context.synchronize(self.source_dir, self.destination_dir, self.context.source_host,
                                 self.context.dest_host, self.context.local_file, self.context.remote_file)

        # --------------------------------  LOCAL modifications  -----------------------------

        Printer.print(f"test, perform all LOCAL modifications in {self.source_dir}", Printer.Verbose)
        for remote in ('New', 'Deleted'):
            self.build_one_test_file(self.context.source_host, self.source_dir, "New", remote)

        self.context.source_host.remove_file(self.source_dir + "/to_be_removed/file1.txt", list_only)
        self.context.source_host.remove_file(self.source_dir + "/to_be_removed/file2.txt", list_only)
        self.context.source_host.remove_dir(self.source_dir + "/to_be_removed", list_only)
        self.context.source_host.move_file(self.source_dir + "/to_be_moved",
                                           self.source_dir + "/after_move", list_only)

        Printer.print(f"test, perform all REMOTE modifications in {self.destination_dir}", Printer.Verbose)
        self.context.dest_host.mkdir(self.destination_dir + "/remote_test")
        self.context.dest_host.build_one_file(self.destination_dir + "/remote_test/file1.txt", ["Created on remote"])
        self.context.dest_host.build_one_file(self.destination_dir + "/remote_test/test2.txt", ["Created on remote"])

        self.context.dest_host.remove_file(self.destination_dir + "/to_be_removed2/file1.txt", list_only)
        self.context.dest_host.remove_file(self.destination_dir + "/to_be_removed2/file2.txt", list_only)
        self.context.dest_host.remove_dir(self.destination_dir + "/to_be_removed2", list_only)

        if do_to_move:
            for remote in ('Unmodified', 'Changed', 'Deleted', 'Moved', 'ToMove', 'ToMove_Renamed_rem'):
                self.change_one_test_file(self.context.source_host, self.source_dir, "Changed", remote, list_only)
                self.remove_one_test_file(self.context.source_host, self.source_dir, 'Deleted', remote, list_only)
                self.move_one_test_file(self.context.source_host, self.source_dir, "Moved", remote, "Renamed_loc",
                                        list_only)
                self.remove_one_test_file(self.context.source_host, self.source_dir, "ToMove", remote + "Renamed_loc",
                                          list_only)
                self.move_one_test_file(self.context.source_host, self.source_dir, "ToMove", remote, "Renamed_loc",
                                        list_only)
        else:
            for remote in ('Unmodified', 'Changed', 'Deleted', 'Moved'):
                self.change_one_test_file(self.context.source_host, self.source_dir, "Changed", remote, list_only)
                self.remove_one_test_file(self.context.source_host, self.source_dir, 'Deleted', remote, list_only)
                self.move_one_test_file(self.context.source_host, self.source_dir, "Moved", remote, "Renamed_loc",
                                        list_only)

        #

        for local in ('New', 'Deleted'):
            self.build_one_test_file(self.context.dest_host, self.destination_dir, local, "New")

        if do_to_move:
            for local in ('Unmodified', 'Changed', 'Deleted', 'Moved', 'ToMove'):
                self.change_one_test_file(self.context.dest_host, self.destination_dir, local, "Changed", list_only)
                self.remove_one_test_file(self.context.dest_host, self.destination_dir, local, 'Deleted', list_only)
                self.move_one_test_file(self.context.dest_host, self.destination_dir, local, "Moved", "Renamed_rem",
                                        list_only)
                self.remove_one_test_file(self.context.dest_host, self.destination_dir, local, 'ToMove_Renamed_rem',
                                          list_only)
                self.move_one_test_file(self.context.dest_host, self.destination_dir, local, "ToMove", "Renamed_rem",
                                        list_only)
        else:
            for local in ('Unmodified', 'Changed', 'Deleted', 'Moved'):
                self.change_one_test_file(self.context.dest_host, self.destination_dir, local, "Changed", list_only)
                self.remove_one_test_file(self.context.dest_host, self.destination_dir, local, 'Deleted', list_only)
                self.move_one_test_file(self.context.dest_host, self.destination_dir, local, "Moved", "Renamed_rem",
                                        list_only)

        Printer.print(f"test: SYNCHRONIZATION of '{self.source_dir} and {self.destination_dir}'", Printer.Verbose)
        self.context.synchronize(self.source_dir, self.destination_dir, self.context.source_host,
                                 self.context.dest_host, self.context.local_file, self.context.remote_file)

        # Now, check that the final status is OK on both sides
        Printer.print(f"test: CHECKING status of all files", Printer.Verbose)
        current_remote_files = {}
        current_remote_inodes = {}
        self.context.dest_host.get_current(self.destination_dir, self.source_dir, self.destination_dir,
                                           current_remote_files, current_remote_inodes, ignore=False)

        current_local_files = {}
        current_local_inodes = {}
        self.context.source_host.get_current(self.source_dir, self.source_dir, self.destination_dir,
                                             current_local_files, current_local_inodes, ignore=False)

        # build the list of files that SHOULD be here, and only those
        # 'Unmodified', 'Changed', 'Deleted', 'Moved_Renamed_rem', 'Moved_Renamed_loc', 'Moved', 'New'

        file_list = [self.build_test_filename("", 'Deleted', "Changed"),
                     self.build_test_filename("", 'Deleted', "Moved_Renamed_rem"),
                     self.build_test_filename("", 'Deleted', "New"),
                     self.build_test_filename("", "Changed", 'Deleted'),  # No conflict here
                     self.build_test_filename("", "Changed", "Changed"),
                     self.build_test_filename("", "Changed", "Changed", self.context.source_host.postfix),
                     self.build_test_filename("", "Changed", "Moved"),
                     self.build_test_filename("", "Changed", "Moved_Renamed_rem"),
                     self.build_test_filename("", "Changed", "Unmodified"),
                     self.build_test_filename("", "Moved", "Deleted_Renamed_loc"),
                     self.build_test_filename("", "Moved", "Changed"),
                     self.build_test_filename("", "Moved", "Changed_Renamed_loc"),
                     self.build_test_filename("", "Moved", "Moved_Renamed_loc"),
                     self.build_test_filename("", "Moved", "Moved_Renamed_rem"),
                     self.build_test_filename("", "Moved", "Unmodified_Renamed_loc"),
                     self.build_test_filename("", "New", 'Deleted'),
                     self.build_test_filename("", "New", "New"),
                     self.build_test_filename("", "Unmodified", "Changed"),
                     self.build_test_filename("", "Unmodified", "Moved_Renamed_rem"),
                     self.build_test_filename("", "Unmodified", "Unmodified"),
                     '.birsync-ignore',
                     'other_subdir/test.txt',
                     'remote_test/file1.txt',
                     "file with ' eacute and ç cedilla é",
                     "dir with é/typical-file.txt",
                     "dir with é/non typical-file with ç.txt",
                     "dir with spaces/non usual-file.txt",
                     "dir with spaces/usual-file.txt",
                     "after_move/file1.txt",
                     "after_move/file2.txt"
                     ]

        # these files are created on local, and not synchronized due to rules
        files_local_only = ["Thumbs.db",
                            '@eaDir/test.txt',
                            'dir with spaces/@eaDir/test.txt']

        # these files are created on remote, and not synchronized due to rules
        files_remote_only = ["subdir/test.txt",  # subdir rule
                             "subdir/test2.txt",  # subdir rule
                             "other_subdir/test2.txt",  # test2 rule
                             "remote_test/test2.txt"  # test2 rule
                             ]

        # these files are temporary, can be here or not depending on settings
        temp_files = [".birsync_from_files", ".birsync_to_files"]

        if not self.context.source_host.is_local() or not self.context.dest_host.is_local():
            file_list.append(self.build_test_filename("", "New", "New", self.context.source_host.postfix))

        verbose_level = Printer.Debug if list_only else Printer.Normal
        for file in current_local_files.keys():
            descriptor = current_local_files[file]
            filename = descriptor.relative_filename
            if filename not in file_list + files_local_only + temp_files:
                Printer.print(f"ERROR test: local '{filename}' not expected", verbose_level)

        for file in current_remote_files.keys():
            descriptor = current_remote_files[file]
            filename = descriptor.relative_filename
            if filename not in file_list + files_remote_only + temp_files:
                Printer.print(f"ERROR test: remote {filename}' not expected", verbose_level)

        for filename in file_list + files_local_only:
            if filename not in current_local_files:
                Printer.print(f"ERROR test: '{filename}' not in local dir", verbose_level)

        for filename in file_list + files_remote_only:
            if filename not in current_remote_files:
                Printer.print(f"ERROR test: '{filename}' not in remote dir", verbose_level)

        ign_name = self.context.source_host.local_filename(self.source_dir+'/to_be_ignored')
        if not self.context.source_host.dir_exists(ign_name):
            Printer.print(f"ERROR test: '/to_be_ignored' dir not in local dir", verbose_level)
        ign_name = self.context.source_host.local_filename(self.source_dir + '/to_be_ignored/twice')
        if not self.context.source_host.dir_exists(ign_name):
            Printer.print(f"ERROR test: '/to_be_ignored/twice' dir not in local dir", verbose_level)

        if not keep_files:
            self.context.source_host.remove_dir(self.source_dir, verbose_level)
            self.context.dest_host.remove_dir(self.destination_dir, verbose_level)

        Printer.print('test end', verbose_level)


# -----------------------------------------------------------------
# BirsyncCmdline
# -----------------------------------------------------------------


class BirsyncCmdline:

    # --------------------------------------------------------
    # Build rsync argument list
    # --------------------------------------------------------
    # rsync -h
    # -P                          same as --partial --progress
    # -u, --update                skip files that are newer on the receiver
    # -c, --checksum              skip based on checksum, not mod-time & size

    # Not used
    #     --delete                delete extraneous files from destination dirs
    # -a, --archive               archive mode; equals -rlptgoD (no -H,-A,-X)
    # -o, --owner                 preserve owner (super-user only)
    # -g, --group                 preserve group
    # -D                          same as --devices --specials
    # --modify-window=1 because MSwindows has a time resolution of 2 seconds.
    # -o, --owner                 preserve owner (super-user only)
    # -g, --group                 preserve group
    #
    #
    # rsync SOURCE DESTINATION
    # Source with an ending /
    # Destination without an ending/ à la fin
    # copy from SOURCE to DESTINATION
    #
    # rsync -vrPEptuC --port 2322  /cygdrive/c/TEST/ rsyncuser@192.168.1.16:/volume1/TEST

    @staticmethod
    def build_rsync_options(arg_list):
        options = "-PucrEtCpg"
        no_options = ""
        opt_list = []

        arg_list.rsync_long_options = arg_list.rsync_long_options or []
        for long_opt in arg_list.rsync_long_options:
            if '--' + long_opt not in opt_list:
                opt_list += ['--' + long_opt]

        arg_list.rsync_short_options = arg_list.rsync_short_options or ""
        for letter in arg_list.rsync_short_options:
            if letter not in options:
                options += letter
            if letter in no_options:
                no_options.replace(letter, '')

        arg_list.rsync_no_short_options = arg_list.rsync_no_short_options or ""
        for letter in arg_list.rsync_no_short_options:
            if letter in options:
                options.replace(letter, '')
            if letter not in no_options:
                no_options += letter

        for letter in no_options:
            opt_list += ['--no-' + letter]

        if arg_list.dry_run or 'n' in options or '--dry-run' in opt_list:
            opt_list += ['--dry-run']
            options += 'i'

        opt_list += ["--chmod=ugo=rwX", "--omit-dir-times"]
        # --fake-super ???  store/recover privileged attrs using xattrs
        # --chmod=CHMOD           affect file and/or directory permissions

        if options != "":
            opt_list += [options]
        # --chmod=ugo=rwX
        return opt_list

    def __init__(self):
        """Create Birsync from Command line options"""
        self.parser = self.build_args_list()
        self.args = self.parser.parse_args()
        self.rsync_options: [str] = self.build_rsync_options(self.args)

        local_user = None
        local_hostname = self.args.source_host
        local_port = None
        remote_user = None
        remote_hostname = self.args.dest_host
        remote_port = None

        hostname = self.args.hostname or platform.node()

        if self.args.source_host is not None:
            local_port = str(self.args.port) if self.args.port else None
            local_user = self.args.user
            if self.args.dest_host is not None:
                Printer.print(f"ERROR ARGUMENTS: cannot have simultaneously --source-host and --dest-host")
                exit(-1)
        elif self.args.dest_host is not None:
            remote_port = str(self.args.port) if self.args.port else None
            remote_user = self.args.user

        verbose = PrintDebug.Normal
        if self.args.verbose_silent:
            verbose = PrintDebug.Silent
        elif self.args.verbose_quiet:
            verbose = PrintDebug.Quiet
        elif self.args.verbose:
            verbose = PrintDebug.Verbose
        elif self.args.verbose_debug:
            verbose = PrintDebug.Debug
        elif self.args.verbose_deep:
            verbose = PrintDebug.Deep

        bash = self.args.bash
        ssh = self.args.ssh
        rsync = self.args.rsync
        postfix = self.args.stamp

        if self.args.full_help:
            BirsyncCmdline.full_help()

        if self.args.licence:
            print("""This program is licenced under the European Union Public Licence v1.2
The full text of EUPL V1.2 can be found, in several languages at:
https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12""")
            exit(0)

        self.context = Birsync(source_user=local_user, source_hostname=local_hostname, source_port=local_port,
                               source_dir=self.args.source,
                               dest_user=remote_user, dest_hostname=remote_hostname, dest_port=remote_port,
                               dest_dir=self.args.dest,
                               rsync_options=self.rsync_options,
                               bash=bash,
                               ssh=ssh,
                               rsync=rsync,
                               verbose=verbose,
                               postfix=postfix,
                               hostname=hostname,
                               list_only=self.args.dry_run,
                               parse_only=self.args.parse_only,
                               self_test=self.args.self_test)

    @staticmethod
    def full_help():
        Printer.print("""" 
        # birsync

Python lib & tool for *bidirectional* synchronization between directories, 
using rsync and rsh

Can be used as a python module
Can be used as a standalone Python script

Assumptions on remote host: rsync (daemon not necessary), ls, rm, rmdir, ssh, cat, bash
Assumptions on local host:  the same + python 3.6 Tested with windows + cygwin + bash + rsync

Manages:
    Bidirectional synchronization
    the status of previous synchronization is kept in a file, to detect special cases
    special cases: File deletion, rename (detected through inode) on one end 
        are reproduced on the other end,
        unless conflict with the actions on the other end?
    conflicts: files are modified differently on both ends
    logs (without rotation)
    multiple synchronization on same repository, e.g. with different hosts
    moving a directory does not impact the synchronization status
    .birsync-ignore file, at the root of the source directory, specifies files/Folders to be ignored.
    self test
    files/directories with space and accent etc letters.
    
Used to synchronize a PC with a Synology NAS, as a lightweight replacement for Cloud-Station-Drive 

# ---------------------------------------------------------------------
# Outline of algorithm

Step 1: build status of files
    rsync --dryrun source destination
    rsync --dryrun destination source
    the output of dryrun is parsed, and leads to the following:
    list of files, each with the dryrun "decision", and the reason for it
    list of directories
    Only the files and directories that need a transfer are listed 
   
Step 2: get the current and previous status for each file 
    'status' for a file is the output of ls -algi
    'current' is now, Current status is read for the whole tree
    'previous' is 'at the time of the last synchronization'
        it is stored, directory per directory, in a file.
    
Step 3:
    for each file, decide about the action 
    execute it (delete, move)
    add it to list of Files that need transfer in one or the other direction (or none)
    
Step 4:
    rsync source destination explicit-list-of-files-to-direction
    rsync destination source explicit-list-of-files-from-direction
    NB: the files are kept, as a log of the actual actions
   
Step 5:
    synchronize the directory structure by removing the EMPTY directorie that are available
    on one end and not in the other
    Due to ignored files, it is difficult to know if a directory is realy empty without testing
      
Step 6:
    build the new "previous" status on source and destination
    

# ---------------------------------------------------------------------
# Decision algorithm

After comparing PREVIOUS and CURRENT status
Each file can be in one of the following states, with their 1 char display:
    C Created       i.e. was not existing in the previous synchronization 
    U Unmodified    
    M Modified 
    D Deleted       i.e. was available in PREVIOUS and is absent in CURRENT
    V Moved-OUT     i.e. was available in PREVIOUS, has changed to 'Renamed' aka is the old name     
    R Renamed-IN    i.e. was not in PREVIOUS, is available in CURRENT, this is the new name.   
    A Absent        not existing in PREVIOUS and CURRENT
    E ERROR 

The potential Decision for each file, with their 1 char display:
    Skip     = rsync will not change this file on either ends, because unmodified
    Src2Dst  > rsync will copy from source to destination
    Dst2Src  < rsync will copy from source to destination
    Conflict C the Source file will be renamed locally, then files will be synchronized in both directions
    RmSrc    D Delete on the source, because was deleted on the destination
    RmDst    d delete on the destination, because was deleted on the source
    MvSrc    M move (i.e. change name) on the source, because was renamed on destination
    MvDst    m Move (i.e. change name) on the destination, because was renamed on source
    Null     # No decision is necessary, rsync will not manage this file
    Error    ! is an internal error, it should never happen


Which leads to the following table:
                  destination
                  C U M D V R A
                  
   S  Created     C C C > > C >   
   o  Unmodified  C = < D = C >   
   u  Modified    C > C > > C C   
   r  Deleted     < d < # # < #   
   c  V Moved out < = < # # < #   
   e  Renamed in  C C C > > C m   
      Absent      < < < # # M #   
  
 Cases that are not obvious:
    V+U: old-file has been moved out, to new-file
        if new-file is in R+A case and on source, new-file == old-file (same date, same size)
        then decision = #  there is nothing to do, because the decision on new-file is m (move src)
        else decision = D (del on source) because the decisionon new-file is > 
        
    R+A: on source, old-file has been moved out, to new-file
        if old-file is in V+U case and, on source new-file == old-file
        then decision = M (move src)
        else decision = > (copy to dest)
            
# ---------------------------------------------------------------------
# Display in verbose mode

 in verbose mode, the decision for each file is displayed with status line of the following format
 decision: TtRCc?  'filename'  -->/<-- 'alternate-filename'
 with:
 T = Transition on source
 t = transition on destination
 R = rsync dryrun char for the file, in the source to destination direction
 C = Change reason on source 
 r = rsync dryrun char for the file, in the destination to source direction 
 c = change reason on destination
 D = decision
 The 'alternate filename' is the new/old name for the file
 
 where C/c change reasons is:
 . = no change
 i = inode
 d = date
 n = path
 s = size
 o = rsync detected a change on source in the --> direction
 O = rsync detected a change on destination in --> direction
 p = rsync detected a change on source in the <-- direction
 P = rsync detected a change on destination in <-- direction
 

# ---------------------------------------------------------------------
# HOW TO to initiate the SSH connection with the linux host

ssh-keygen -t rsa -b 2048
ssh-copy-id username@hostname -p portnumber
ssh ssh://username@hostname:portnumber # will ask for username password the first time


# ---------------------------------------------------------------------
# Management of .birsync-ignore

This file is used only at the root of the synchronized directories,
because rsync cannot manage it with several directories
if you really want a finer grain, you need to rsync directory per directory
The file is always read on the local host, independently on the synchronization direction

# ---------------------------------------------------------------------
# Management of history database

1 file is created in *root directory*, on each host
.birsync_{host}_{hash}_to_{peer}_{peer_hash} 
where {host} and {peer} are the hostnames
{hash} and {peer_hash} are SHA1 hashes of the root path of synchronization
This naming allows:
* previous status can be used if the source/destination order is changed.
* previous status are kept different, even is the host is identical, since the root dirs cannot be also identical
* status files are different if the tree synchronized is different

The status files hold the status of the directory after the LAST synchronization
So, they are updated when the synchronization is completed.
If not prevented by the ignore rules, these files will be synchronized.
since the update is done after synchronization, the synchronized version is overwritten.

The state of each file (see "Decision algorithm") is done by comparing 
the .rsync_remote/local_dir_{hostname} with the current status BEFORE the synchronization
       
        --reset-database


# ---------------------------------------------------------------------
# Differences between source and destination

The synchronization is bidirectional, so, there are very little differences between source and destination:
* in case of conflict,  destination is unchanged and source is moved to file_{stamp}.extension 

# ---------------------------------------------------------------------
# Examples of SELF TESTS
#   the same arguments can always be used with --parse-only and/or --dry-run and --verbose-XXX options
 
--self-test  
  # no directory specified: a temp directory is created, and src, dst directories in it
  # format is src_{'%Y-%m-%d_T%H-%M-%S'}
    
--self-test --keep-test-file 
  # idem. test files are kept
  
--self-test --source "C:\\\\Windows\\\\Temp" --dest "C:\\\\Windows\\\\Temp" 
  # src and dst directories are created in the specified directories
  
--self-test --source "C:\\\\Windows\\\\Temp" --dest "/tmp"   --dest-host "my_host" --user "my_user" --port 2222  
--self-test --dest "C:\\\\Windows\\\\Temp" --source "/tmp" --source-host "my_host" --user "my_user" --port 2222 
  # self-test is OK in both directions

# ---------------------------------------------------------------------
# Examples of normal usages
#   the same arguments can always be used with --parse-only and/or --dry-run and --verbose-XXX options

--source "D:\\\\win_dir" --dest "/volume1/linux_dir"  --dest-host "my_host" --user "my_user" --port 2222
--dest "D:\\\\win_dir\\\\" --source "/volume1/linux_dir/"  --source-host "my_host" --user "my_user" --port 2222
  # both directions are possible
  # assuming the 1st host is running windows and cygwin
  # the ending \\ or / is removed
   
--source "/cygdrive/c/win_dir2" --dest "/volume1/linux_dir2" --dest-host "my_host" --user "my_user" --port 2322 
  # alternative syntax for windows/cygwin host

# --------------------------------------------------------------------
# Licence

This project is licenced under the European Union Public Licence v1.2

The full text of EUPL V1.2 can be found, in several languages at:
https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12

        """)
        exit(0)

    @staticmethod
    def build_args_list():
        my_parser = argparse.ArgumentParser(description='bidirectional synchronization of 2 directories')

        my_parser.add_argument('--user', '-u', help="remote user", type=str)
        my_parser.add_argument('--source-host', help="source host", type=str)
        my_parser.add_argument('--dest-host', help="destination host", type=str)

        my_parser.add_argument('--bash', help="location of bash executable", type=str)
        my_parser.add_argument('--ssh', help="location of ssh executable", type=str)
        my_parser.add_argument('--rsync', help="location of rsync executable", type=str)
        my_parser.add_argument('--stamp', help="time stamp for conflict files (use only in case of trouble)", type=str)
        my_parser.add_argument('--full-help', help="prints the complete help, and exits", action='store_true')
        my_parser.add_argument('--licence', help="prints the LICENCE", action='store_true')

        my_parser.add_argument('--self-test', help="performs end-to-end self tests of the program", action='store_true')
        my_parser.add_argument('--unitary-test', help="performs unitary self tests of the program", action='store_true')
        my_parser.add_argument('--keep-test-files', help="keep test files generated during tests", action='store_true')
        my_parser.add_argument('--verbose-silent', help="never print", action='store_true')
        my_parser.add_argument('--verbose-quiet', help="only errors are print", action='store_true')
        my_parser.add_argument('--verbose', '-v', help="increased verbosity", action='store_true')
        my_parser.add_argument('--verbose-debug', help="verbosity for debug", action='store_true')
        my_parser.add_argument('--verbose-deep', help="maximal verbosity", action='store_true')
        my_parser.add_argument('--source', help="source directory", type=str, dest='source')
        my_parser.add_argument('--dest', help="destination directory", type=str, dest='dest')
        my_parser.add_argument('--postfix', help="postfix for conflict files", type=str)
        my_parser.add_argument('--dry-run', help="does not copy or remove files, just list actions",
                               action='store_true')
        my_parser.add_argument('--port', help="set remote port number", type=int)
        my_parser.add_argument('--parse-only', help="parse cmdline arguments, echo them, and exit",
                               action='store_true')

        my_parser.add_argument('--hostname', help="overwrites the default hostname where python is run", type=str)

        # options to manage rsync
        # standard options are -vrPEptuC

        my_parser.add_argument('--rsync-long-option',
                               help="adds '--' to the value, and pass it to rsync. Can be used multiple times",
                               dest='rsync_long_options', action='append')
        my_parser.add_argument('--rsync-short-option', help="values are added to rsync short options",
                               dest='rsync_short_options')
        my_parser.add_argument('--rsync-no-short-option', help="values are removed to rsync short options",
                               dest='rsync_no_short_options')

        return my_parser

    def execute(self):
        if self.args.unitary_test:
            unitary_test(self.context)
        elif self.args.self_test:
            tester = Tester(self.context)
            tester.end_to_end_test(self.args.keep_test_files, self.args.dry_run)
        else:
            self.context.synchronize(self.context.source_host.directory,
                                     self.context.dest_host.directory,
                                     self.context.source_host, self.context.dest_host,
                                     self.context.local_file, self.context.remote_file)


global_cmd_line = BirsyncCmdline()
global_cmd_line.execute()

# test scripts:
#
#  --self-test
#  --self-test --keep-test-file --dry-run
#  --self-test --source "C:\Windows\Temp" --dest "C:\Windows\Temp"
#  --self-test --source "C:\Windows\Temp" --dest "/tmp"   --dest-host NAS --user "my_user" --port 2222
#  --self-test --dest "C:\Windows\Temp" --source "/tmp" --source-host NAS --user "my_user" --port 2222
